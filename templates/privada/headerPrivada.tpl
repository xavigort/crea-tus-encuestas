		<div id="header">
				<div id="logoContainer">
					<div id="logo"><a href="{$baseURL}"><img alt="Crea tus encuestas" src="{$baseURL}/images/logo.jpg" /></a></div>
				</div>
				
				<div id="login">
					<a class="loginLink" href="{$baseURL}/cerrar-sesion/"><span class="loginButton">Cerrar sesi&oacute;n</span></a>
					{if $op neq 'listadoEncuestas'}	
						<a class="loginLink" href="{$baseURL}/listado-encuestas/"><span class="loginButton">Tu &aacute;rea privada</span></a>
					{else}
						<a class="loginLink" href="{$baseURL}/editar-perfil/"><span class="loginButton">Editar tu perfil</span></a>
					{/if}
					<p class="saludo">Bienvido/a {$usuarioPrivado->getNombre()},</p>
				</div>
				
				<div id="menu">					
						<a class="menuLink" href="{$baseURL}/demo/"><span class="menuButton">Demo</span></a>
						<a class="menuLink" href="{$baseURL}/plantillas/"><span class="menuButton">Plantillas</span></a>
						<a class="menuLink" href="{$baseURL}/ultimas-encuestas/"><span class="menuButton">&Uacute;ltimas encuestas</span></a>
						<a class="menuLink" href="{$baseURL}/blog/"><span class="menuButton">Blog</span></a>					
				</div>
			</div>