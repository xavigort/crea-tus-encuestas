
	<!-- Header and metas -->
		<title>&Aacute;rea privada - Crea Tus Encuestas</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="{$baseURL}/css/privada.css" media="screen" />

		<!-- FUNCTIONS -->
		<script src="{$baseURL}/js/library.js" type="text/javascript"></script>
		<script src="{$baseURL}/js/backend.js" type="text/javascript"></script>

		<!-- JQUERY -->
		<script src="{$baseURL}/js/jquery.min.js" type="text/javascript"></script>

		<!-- CUFON -->
		<script src="{$baseURL}/js/cufon/cufon-yui.js" type="text/javascript"></script>
		<script src="{$baseURL}/js/cufon/DINSchrift_700.font.js" type="text/javascript"></script>

		<!-- THICKBOX -->
		<script src="{$baseURL}/js/thickbox/thickbox.js" type="text/javascript" ></script>
		<link rel="stylesheet" href="{$baseURL}/css/thickbox/thickbox.css" type="text/css" media="screen" />

		<!-- DRAGGABLE THICKBOX -->
		<script src="{$baseURL}/js/drag/drag.js" type="text/javascript" ></script>

		<!-- TOOLTIPS -->
		<script src="{$baseURL}/js/tooltip/tooltip.js" type="text/javascript" ></script>
		<link rel="stylesheet" href="{$baseURL}/css/tooltip/tooltip.css" type="text/css" media="screen" />

		<!-- COLOR PICKER -->
		<script type="text/javascript" language="javascript" src="{$baseURL}/js/colorPicker/colorPicker.js"></script>
		<link rel="stylesheet" href="{$baseURL}/css/colorPicker/colorPicker.css" type="text/css"></link>

		{literal}

			<script type="text/javascript">

				/****************
					LISTA ENCUESTA
				****************/

				Cufon.replace('h1');
				Cufon.replace('h2');
				Cufon.replace('h3');
				Cufon.replace('h4');

				/** Normal efect **/
				Cufon.replace('#navegacion');
				Cufon.replace('.webMapList li');
				Cufon.replace('#copyright');
				Cufon.replace('.moduleTitle');
				Cufon.replace('.formFieldTitleContainer');

				/** Hover link efect **/
				Cufon.replace('.registerLinks', {hover: true});
				Cufon.replace('.menuLink', {hover: true});
				Cufon.replace('.loginLink', {hover: true});
				Cufon.replace('.webMapLink', {hover: true});


				/****************
					ALTA ENCUESTA
				****************/

				/** Hover link efect **/
				Cufon.replace('.paso', {hover: true});
				Cufon.replace('.completadoLink', {hover: true});
			</script>

		{/literal}

