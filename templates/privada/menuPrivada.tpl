		<div id="modulesContent">
							
				<!-- User Info -->
				
					<div class="infoModule">
						<p class="moduleTitle">Informaci&oacute;n encuestas</p>
						<p class="titleText">Total encuestas: 6</p>
						<p class="sumaryText">Borradores: 3</p>
						<p class="sumaryText">Publicadas: 1</p>
						<p class="sumaryText">Cerradas: 2</p>
						<p class="spacer10">&nbsp;</p>
						<p class="titleText">&Uacute;ltimo acceso: 01/02/2010</p>
					</div>

				<!-- End User Info -->
				
				<!-- Add items -->
				
					<div class="infoModule">
						<p class="moduleTitle">A&ntilde;adir</p>
						<p class="separador"></p>
						<p class="addItem"><a href="#">Nueva encuesta</a></p>
						<p class="separador"></p>
						<p class="addItem"><a href="#">Nueva plantilla</a></p>
						<p class="separador"></p>
						<p class="addItem"><a href="#">Nuevo modelo</a></p>		
					</div>
				
				<!-- End Add items -->
				
				<!-- Support items -->
				
					<div class="infoModule">
						<p class="moduleTitle">Soporte</p>
						
						<p class="separador"></p>
						<p class="supportItem"><a href="#">Nueva encuesta</a></p>
						<p class="separador"></p>
						<p class="supportItem"><a href="#">Nueva plantilla</a></p>
						<p class="separador"></p>
						<p class="supportItem"><a href="#">Nuevo modelo</a></p>
						
					</div>
				
				<!-- End Support items -->
				
			</div>