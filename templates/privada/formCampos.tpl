﻿<div id="fieldFormContent">
	<form id="nuevoCampo" action="#" method="post">
				
		<div class='formFieldTitleContainer'><p class='formFieldTitle'><img src="{$baseURL}/images/backend/altaCampo1.gif" /><span class="fieldLegend" id="fieldLegend"></span></p></div>				
		
		<div id="fields">
			<a href="#"><span id="field1" class="campo campoTipo1" onClick="showField(1)" onMouseOver="showFieldLegend(1)" onMouseOut="clearFieldLegend()">Campo de texto</span></a>
			<a href="#"><span id="field2" class="campo campoTipo2" onClick="showField(2)" onMouseOver="showFieldLegend(2)" onMouseOut="clearFieldLegend()">P&aacute;rrafo</span></a>
			<a href="#"><span id="field3" class="campo campoTipo3" onClick="showField(3)" onMouseOver="showFieldLegend(3)" onMouseOut="clearFieldLegend()">Desplegable</span></a>
			<a href="#"><span id="field4" class="campo campoTipo4" onClick="showField(4)" onMouseOver="showFieldLegend(4)" onMouseOut="clearFieldLegend()">Selector m&uacute;ltiple</span></a>
			<a href="#"><span id="field5" class="campo campoTipo5" onClick="showField(5)" onMouseOver="showFieldLegend(5)" onMouseOut="clearFieldLegend()">Selector &uacute;nico</span></a>
			<a href="#"><span id="field6" class="campo campoTipo6" onClick="showField(6)" onMouseOver="showFieldLegend(6)" onMouseOut="clearFieldLegend()">Campo de fecha</span></a>
		</div>
		
		<div class="clear"></div>
		<div class="spacer10"></div>
		
		<div id="step2" class='formFieldTitleContainer vHidden'>					
			<p class='formFieldTitle'><span id="titleContainer"><img src="{$baseURL}/images/backend/altaCampo2.gif" /></p>
		</div>
		
		<div id="formFieldsContainer">
		
			<!-- Campo 0 para el fadeOut -->
			<div class="fieldContainer vHidden dNone" id="fieldContainer0">&nbsp;</div>
			
			<!-- Campo 1 - Campo de Texto -->
			<div class="fieldContainer vHidden dNone" id="fieldContainer1">
				
					<input type="hidden" name="tipoCampo" value="1" />
					<div class="leftFormCol">
						<p class="formFieldParagraph">
							<label for="nombreCampo1">
							<span class="vspacer5"></span>
							<strong>Nombre</strong> del campo: </label><input id="nombreCampo1" type="text" name="nombreCampo1" value="Nombre" />
						</p>
						<p class="formFieldParagraph">
							<label>&iquest;Se trata de un campo <strong>obligatorio</strong>?</label><br/>
							<span class="vspacer5"></span>
							<input type="radio" id="obligatorio1_0" name="obligatorio1" value="1" /><span class="hspacer5">&nbsp;</span><label for="obligatorio1_0">Si, es obligatorio</label>
							<span class="hspacer10">&nbsp;</span>
							<input type="radio" id="obligatorio1_1" name="obligatorio1" value="0" checked /><span class="hspacer5">&nbsp;</span><label for="obligatorio1_1">No, es optativo</label>
						</p>
						<p class="formFieldParagraph">
							<label>&iquest;Qu&eacute; <strong>tama&ntilde;o</strong> de campo deseas?</label><br/>
							<span class="vspacer5"></span>
							<input type="radio" id="dimensionCampo1_0" name="dimensionCampo1" value="0" /><span class="hspacer5">&nbsp;</span><label for="dimensionCampo1_0">Peque&ntilde;o</label>
							<span class="hspacer10">&nbsp;</span>
							<input type="radio" id="dimensionCampo1_1" name="dimensionCampo1" value="1" checked/><span class="hspacer5">&nbsp;</span><label for="dimensionCampo1_1">Medio</label>
							<span class="hspacer10">&nbsp;</span>
							<input type="radio" id="dimensionCampo1_2" name="dimensionCampo1" value="2" /><span class="hspacer5">&nbsp;</span><label for="dimensionCampo1_2">Grande</label>
						</p>
					</div>
					<div class="rightFormCol">
						<p class="formFieldLabel">
							<label>&iquest;Qu&eacute; <strong>tipo </strong> de informaci&oacute;n contendr&aacute;?</label>
						</p>
						<p class="hspacer15">
							<input type="radio" id="tipoTexto1_0" name="tipoTexto1" value="0" checked /><span class="hspacer5">&nbsp;</span><label for="tipoTexto1_0">Texto y n&uacute;meros</label><br/>
							<input type="radio" id="tipoTexto1_1" name="tipoTexto1" value="1" /><span class="hspacer5">&nbsp;</span><label for="tipoTexto1_1">Solo n&uacute;meros</label><br/>
							<input type="radio" id="tipoTexto1_2" name="tipoTexto1" value="2" /><span class="hspacer5">&nbsp;</span><label for="tipoTexto1_2">Un email</label><br/>
							<input type="radio" id="tipoTexto1_3" name="tipoTexto1" value="3" /><span class="hspacer5">&nbsp;</span><label for="tipoTexto1_3">Una url</label>
						</p>								
					</div>
				
			</div>
			
			<!-- Campo 2 - Párrafo -->
			
			<div class="fieldContainer vHidden dNone" id="fieldContainer2">
				
				<input type="hidden" name="tipoCampo" value="2" />
				<div class="leftFormCol">
					<p class="formFieldParagraph">
						<label for="nombreCampo2">
						<span class="vspacer5"></span>
						<strong>Nombre</strong> del campo: </label><input id="nombreCampo2" type="text" name="nombreCampo2" value="Nombre" />
					</p>
					<p class="formFieldParagraph">
						<label>&iquest;Se trata de un campo <strong>obligatorio</strong>?</label><br/>
						<span class="vspacer5"></span>
						<input type="radio" id="obligatorio2_0" name="obligatorio2" value="1" /><span class="hspacer5">&nbsp;</span><label for="obligatorio2_0">Si, es obligatorio</label>
						<span class="hspacer10">&nbsp;</span>
						<input type="radio" id="obligatorio2_1" name="obligatorio2" value="0" checked /><span class="hspacer5">&nbsp;</span><label for="obligatorio2_1">No, es optativo</label>
					</p>
					<p class="formFieldParagraph">
						<label>&iquest;Qu&eacute; <strong>tama&ntilde;o</strong> de campo deseas?</label><br/>
						<span class="vspacer5"></span>
						<input type="radio" id="dimensionCampo2_0" name="dimensionCampo2" value="0" /><span class="hspacer5">&nbsp;</span><label for="dimensionCampo2_0">Peque&ntilde;o</label>
						<span class="hspacer10">&nbsp;</span>
						<input type="radio" id="dimensionCampo2_1" name="dimensionCampo2" value="1" checked/><span class="hspacer5">&nbsp;</span><label for="dimensionCampo2_1">Medio</label>
						<span class="hspacer10">&nbsp;</span>
						<input type="radio" id="dimensionCampo2_2" name="dimensionCampo2" value="2" /><span class="hspacer5">&nbsp;</span><label for="dimensionCampo2_2">Grande</label>
					</p>
				</div>
				
			</div>
			
			<!-- Campo 3 - Desplegable -->
			
			<div class="fieldContainer vHidden dNone" id="fieldContainer3">
				
				<input type="hidden" name="tipoCampo" value="3" />
				<div class="leftFormCol">
					<p class="formFieldParagraph">
						<label for="nombreCampo3">
						<span class="vspacer5"></span>
						<strong>Nombre</strong> del campo: </label><input id="nombreCampo3" type="text" name="nombreCampo3" value="Nombre" />
					</p>
					<p class="formFieldParagraph">
						<label>&iquest;Se trata de un campo <strong>obligatorio</strong>?</label><br/>
						<span class="vspacer5"></span>
						<input type="radio" id="obligatorio3_0" name="obligatorio3" value="1" /><span class="hspacer5">&nbsp;</span><label for="obligatorio3_0">Si, es obligatorio</label>
						<span class="hspacer10">&nbsp;</span>
						<input type="radio" id="obligatorio3_1" name="obligatorio3" value="0" checked /><span class="hspacer5">&nbsp;</span><label for="obligatorio3_1">No, es optativo</label>
					</p>
					<p class="formFieldParagraph">
						<label>&iquest;Qu&eacute; <strong>tama&ntilde;o</strong> de campo deseas?</label><br/>
						<span class="vspacer5"></span>
						<input type="radio" id="dimensionCampo3_0" name="dimensionCampo3" value="0" /><span class="hspacer5">&nbsp;</span><label for="dimensionCampo3_0">Peque&ntilde;o</label>
						<span class="hspacer10">&nbsp;</span>
						<input type="radio" id="dimensionCampo3_1" name="dimensionCampo3" value="1" checked/><span class="hspacer5">&nbsp;</span><label for="dimensionCampo3_1">Medio</label>
						<span class="hspacer10">&nbsp;</span>
						<input type="radio" id="dimensionCampo3_2" name="dimensionCampo3" value="2" /><span class="hspacer5">&nbsp;</span><label for="dimensionCampo3_2">Grande</label>
					</p>
				</div>
				
				<div id="selectListContainer" class="rightFormCol">
					<p class="formFieldLabel">
						<label>Define las opciones de tu desplegable</label>
					</p>
					<p class="hspacer15">
						<div id="selectList">
							<input type="hidden" id="idSelect" value="2">
							<input id="desplegabl3_0" type="text" name="desplegable3[]" value="opcion1" /><span class="vspacer5"></span>
							<input id="desplegable3_1" type="text" name="desplegable3[]" value="opcion2" /><span class="vspacer5"></span>
						</div>
						
						<p class="addButtonContainer"><span class="fieldExplain" onmouseover="showLinkLegend('addOpcion3')" onmouseout="clearLinkLegend('addOpcion3')" onClick="addOption(3);">+</span> <span id="addOpcion3" class="addContainer">A&ntilde;adir opci&oacute;n</span></p>
					</p>								
				</div>
				
				<div id="selectListPremium" class="rightFormCol dNone vHidden">
					<p class="formFieldLabel">Para poder definir <strong>m&aacute;s de 5 opciones</strong> en tu desplegable necesitar&aacute;s una <strong>cuenta premium</strong>.</p>
					<p class="formFieldLabel"><a href="{$baseURL}/" target="_blank">&iexcl;Consigue una cuenta premium ahora!</a></p>							
					<p class="formFieldLabel"><br/><a href="#" onClick="hideVisibleField('selectListPremium');showHiddenField('selectListContainer');">Vuelve a tus opciones</a></p>
				</div>
							
				
			</div>
			
			<!-- Campo 4 - Selector múltiple -->
			
			<div class="fieldContainer vHidden dNone" id="fieldContainer4">
				
				<input type="hidden" name="tipoCampo" value="4" />
				<div class="leftFormCol">
					<p class="formFieldParagraph">
						<label for="nombreCampo4">
						<span class="vspacer5"></span>
						<strong>Nombre</strong> del campo: </label><input id="nombreCampo4" type="text" name="nombreCampo4" value="Nombre" />
					</p>
					<p class="formFieldParagraph">
						<label>&iquest;Se trata de un campo <strong>obligatorio</strong>?</label><br/>
						<span class="vspacer5"></span>
						<input type="radio" id="obligatorio4_0" name="obligatorio4" value="1" /><span class="hspacer5">&nbsp;</span><label for="obligatorio4_0">Si, es obligatorio</label>
						<span class="hspacer10">&nbsp;</span>
						<input type="radio" id="obligatorio4_1" name="obligatorio4" value="0" checked /><span class="hspacer5">&nbsp;</span><label for="obligatorio4_1">No, es optativo</label>
					</p>
					<p class="formFieldParagraph">
						<label>&iquest;Qu&eacute; <strong>alinieaci&oacute;n</strong> deseas para tus opciones?</label><br/>
						<span class="vspacer5"></span>
						<input type="radio" id="posicionCampo4_0" name="posicionCampo4" value="0" checked/><span class="hspacer5">&nbsp;</span><label for="posicionCampo4_0">Horizontal</label>
						<span class="hspacer10">&nbsp;</span>
						<input type="radio" id="posicionCampo4_1" name="posicionCampo4" value="1"/><span class="hspacer5">&nbsp;</span><label for="posicionCampo4_1">Vertical</label>								
					</p>
				</div>
				
				<div id="checkListContainer" class="rightFormCol">
						<p class="formFieldLabel">
							<label>Define las m&uacute;litples opciones</label>
						</p>
						<p class="hspacer15">
							<div id="checkList">
								<input type="hidden" id="idCheck" value="2">
								<input id="desplegable4_0" type="text" name="desplegable4[]" value="opcion1" /><span class="vspacer5"></span>
								<input id="desplegable4_1" type="text" name="desplegable4[]" value="opcion2" /><span class="vspacer5"></span>
							</div>
							<p class="addButtonContainer"><span class="fieldExplain" onmouseover="showLinkLegend('addOpcion4')" onmouseout="clearLinkLegend('addOpcion4')" onClick="addOption(4);">+</span> <span id="addOpcion4" class="addContainer">A&ntilde;adir opci&oacute;n</span></p>
						</p>								
				</div>
				
				<div id="checkListPremium" class="rightFormCol dNone vHidden">
					<p class="formFieldLabel">Para poder definir <strong>m&aacute;s de 5 opciones</strong> en tu selector m&uacute;ltiple necesitar&aacute;s una <strong>cuenta premium</strong>.</p>
					<p class="formFieldLabel"><a href="{$baseURL}/" target="_blank">&iexcl;Consigue una cuenta premium ahora!</a></p>							
					<p class="formFieldLabel"><br/><a href="#" onClick="hideVisibleField('checkListPremium');showHiddenField('checkListContainer');">Vuelve a tus opciones</a></p>
				</div>
				
			</div>
			
			<!-- Campo 5 - Selector único -->
			
			<div class="fieldContainer vHidden dNone" id="fieldContainer5">
				
				<input type="hidden" name="tipoCampo" value="5" />
				<div class="leftFormCol">
					<p class="formFieldParagraph">
						<label for="nombreCampo5">
						<span class="vspacer5"></span>
						<strong>Nombre</strong> del campo: </label><input id="nombreCampo5" type="text" name="nombreCampo5" value="Nombre" />
					</p>
					<p class="formFieldParagraph">
						<label>&iquest;Qu&eacute; <strong>alinieaci&oacute;n</strong> deseas para tus opciones?</label><br/>
						<span class="vspacer5"></span>
						<input type="radio" id="posicionCampo5_0" name="posicionCampo5" value="0" checked/><span class="hspacer5">&nbsp;</span><label for="posicionCampo5_0">Horizontal</label>
						<span class="hspacer10">&nbsp;</span>
						<input type="radio" id="posicionCampo5_1" name="posicionCampo5" value="1" /><span class="hspacer5">&nbsp;</span><label for="posicionCampo5_1">Vertical</label>								
					</p>
				</div>
				
				<div id="radioListContainer" class="rightFormCol">
						<p class="formFieldLabel">
							<label>Define las diferentes opciones</label>
						</p>
						<p class="hspacer15">
							<div id="radioList">
								<input type="hidden" id="idRadio" value="2">
								<input id="desplegable5_0" type="text" name="desplegable5[]" value="opcion1" /><span class="vspacer5"></span>
								<input id="desplegable5_1" type="text" name="desplegable5[]" value="opcion2" /><span class="vspacer5"></span>
							</div>
							
							<p class="addButtonContainer"><span class="fieldExplain" onmouseover="showLinkLegend('addOpcion5')" onmouseout="clearLinkLegend('addOpcion5')" onClick="addOption(5);">+</span> <span id="addOpcion5" class="addContainer">A&ntilde;adir opci&oacute;n</span></p>
						</p>								
				</div>
				
				<div id="radioListPremium" class="rightFormCol dNone vHidden">
					<p class="formFieldLabel">Para poder definir <strong>m&aacute;s de 5 opciones</strong> en tu selector &uacute;nico necesitar&aacute;s una <strong>cuenta premium</strong>.</p>
					<p class="formFieldLabel"><a href="{$baseURL}/" target="_blank">&iexcl;Consigue una cuenta premium ahora!</a></p>							
					<p class="formFieldLabel"><br/><a href="#" onClick="hideVisibleField('radioListPremium');showHiddenField('radioListContainer');">Vuelve a tus opciones</a></p>
				</div>
			
			</div>
			
			<!-- Campo 6 - Campo de fecha -->
			
			<div class="fieldContainer vHidden dNone" id="fieldContainer6">
				<div id="fieldContainerNoPremium">
					<p class="formFieldLabel">Para poder utilizar <strong>campos de fecha</strong> necesitar&aacute;s disponer de una <strong>cuenta premium</strong>.</p>
					<p class="formFieldLabel"><a href="{$baseURL}/" target="_blank">&iexcl;Consigue una cuenta premium ahora!</a></p>
				</div>
			</div>
			
		</div>
		
		<div id="fieldButtons">
			<a class="buttonLink fRight" href="#" onclick="return false;"><span class="menuButton">Cerrar</span></a>				
			<a class="buttonLink fRight" href="#"><span class="menuButton">A&ntilde;adir campo</span></a>
		</div>

	</form>
</div>