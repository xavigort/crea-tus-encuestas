﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head>		
		{include file='./privada/headPrivada.tpl'}
		<!-- jqtranform - Nice Javascript Forms -->
		
		<link rel="stylesheet" type="text/css" href="{$baseURL}/js/jqtransformplugin/jqtransform.css" media="screen" />
		<script src="{$baseURL}/js/jqtransformplugin/jquery.jqtransform.js" type="text/javascript"></script>
		
		{literal}
		
			<script type="text/javascript">
				$(function(){
					$('form').jqTransform({imgPath:'{$baseURL}/js/jqtransformplugin/img/'});
				});
				
			</script>
		
		{/literal}
		
		<!-- Nice Javascript Form validator -->
		
		<link rel="stylesheet" type="text/css" href="{$baseURL}/js/jqValidation/validationEngine.jquery.css" media="screen" />
		<script src="{$baseURL}/js/jqValidation/jquery.validationEngine-en.js" type="text/javascript"></script>
		<script src="{$baseURL}/js/jqValidation/jquery.validationEngine.js" type="text/javascript"></script>
		
		{literal}
			<script type="text/javascript">
				$(document).ready(function() 
				{
					$("#form1").validationEngine()
				});
			</script>	
		{/literal}	

	</head>
	<body>
		<div id="container">
			
			<!-- Header -->
				{include file='./privada/headerPrivada.tpl'}
		
			<!-- End Header -->
			
			<div class="orangeSlash"></div>
			
			<!-- Content -->
	
			<div id="content">
				
				<!-- Modules Col -->
					{include file="./privada/menuPrivada.tpl"}
				<!-- End Modules Col -->
				
				<!-- ContentCol -->
				
				<div id="mainContent">
					
					<div id="mainTitle">
						<h4>Nueva encuesta</h4>
					</div>
					
					<div id="pasos">
						<span class="paso activo">1- Datos b&aacute;sicos</span>
						<span class="paso pendiente">2- Tus preguntas</span>
						<span class="paso pendiente">3- Configuraci&oacute;n</span>
					</div>
					
					<div class="clear"></div>
					
					<div id="formContainer">
					
						<p class="moduleTitle">Para empezar, define los datos b&aacute;sicos de tu encuesta.</p>
						
						<div id="formContent">
							
							<form id="form1" action="{$baseURL}/nueva-encuesta/paso2/" method="post">							
								<div class="rowElem"><label>* Indica el <strong>título</strong> de tu encuesta <span class="fieldExplain" onmouseover="tooltip.show('<strong>lorem ipsum.</strong> dolor sit amet');" onmouseout="tooltip.hide();">?</span></label><input type="text" id="surveyTitle" name="surveyTitle" class="validate[required,length[0,150]]" /></div>
								<div class="rowElem"><label>¿Quieres dejar una <strong>descripción</strong> para la encuesta? <span class="fieldExplain" onmouseover="tooltip.show('<strong>lorem ipsum.</strong> dolor sit amet');" onmouseout="tooltip.hide();">?</span></label> <textarea id="surveyDescription" name="surveyDescription" class=""></textarea></div>
								<div class="rowElem"><label>¿Quieres mostrar el <strong>progreso</strong> en tu encuesta? <span class="fieldExplain" onmouseover="tooltip.show('<strong>lorem ipsum.</strong> dolor sit amet');" onmouseout="tooltip.hide();">?</span></label>
									<input type="radio" id="surveyShowProgress1" name="surveyShowProgress" value="1" /><label class="uniLineal">Si</label>
									<input type="radio" id="surveyShowProgress0" name="surveyShowProgress" value="0" checked /><label class="uniLineal">No</label>
								</div>
								<div class="rowElem"><label>¿Quieres que tus <strong>preguntas se numeren</strong>? <span class="fieldExplain" onmouseover="tooltip.show('<strong>lorem ipsum.</strong> dolor sit amet');" onmouseout="tooltip.hide();">?</span></label>
									<input type="radio" id="surveyEnumerate1" name="surveyEnumerate" value="1" /><label class="uniLineal">Si</label>
									<input type="radio" id="surveyEnumerate0" name="surveyEnumerate" value="0" checked /><label class="uniLineal">No</label>
								</div>
								<div class="rowElem"><label>Define el <strong>mensaje de finalización</strong> de encuesta <span class="fieldExplain" onmouseover="tooltip.show('<strong>lorem ipsum.</strong> dolor sit amet');" onmouseout="tooltip.hide();">?</span></label>
									<!--
									<input type="radio" id="surveyEndingMode0" name="surveyEndingMode" value="0" checked /><label class="uniLineal">Mostrar un mensaje</label>
									<input type="radio" id="surveyEndingMode1" name="surveyEndingMode" value="1"  /><label class="uniLineal">Redirigir a otra URL</label>
									-->
									<div class="rowElem"><input type="text" id="surveyEndingValue" name="surveyEndingValue" class="validate[required,length[0,150]]" /></div>
								</div>
								
									
								<div id="botonera">
									<input type="submit" value="Guardar cambios" />
								</div>
							</form>
							
						</div>
					</div>
				</div>
			</div>		
			
			<!-- End Content -->

			<div class="orangeSlash"></div>
			
		<!-- Footer -->
			{include file='./includes/footer.tpl'}
		<!-- End Footer -->
			
		</div>		
	</body>
</html>