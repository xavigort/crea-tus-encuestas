﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head>		
		{include file='./privada/headPrivada.tpl'}
		<!-- jqtranform - Nice Javascript Forms -->
		
		<link rel="stylesheet" type="text/css" href="{$baseURL}/js/jqtransformplugin/jqtransform.css" media="screen" />
		<script src="{$baseURL}/js/jqtransformplugin/jquery.jqtransform.js" type="text/javascript"></script>
		<script src="{$baseURL}/js/toggle/toggle.js" type="text/javascript"></script>
		
		{literal}
		
			<script type="text/javascript">
				$(function(){
					$('form').jqTransform({imgPath:'{$baseURL}/js/jqtransformplugin/img/'});
				});
				
			</script>
		
		{/literal}
		
		<!-- Nice Javascript Form validator -->
		
		<link rel="stylesheet" type="text/css" href="{$baseURL}/js/jqValidation/validationEngine.jquery.css" media="screen" />
		<script src="{$baseURL}/js/jqValidation/jquery.validationEngine-en.js" type="text/javascript"></script>
		<script src="{$baseURL}/js/jqValidation/jquery.validationEngine.js" type="text/javascript"></script>
		
		{literal}
			<script type="text/javascript">
				$(document).ready(function() 
				{
					$("#form1").validationEngine()
				});
			</script>	
		{/literal}	
		
		{literal}	
			<script type="text/javascript" charset="utf-8">
				// Hide Boxes
				
				$(document).ready(function()
				{				
						$("#newField").hide();						
				});
			</script>
		{/literal}
	</head>
	<body>
	
		<div id="container">
			
			<!-- Header -->
				{include file='./privada/headerPrivada.tpl'}
		
			<!-- End Header -->
			
			<div class="orangeSlash"></div>
			
			<!-- Content -->
	
			<div id="content">
				
				<!-- Modules Col -->
					{include file="./privada/menuPrivada.tpl"}
				<!-- End Modules Col -->
				
				<!-- ContentCol -->
				
				<div id="mainContent">
					
					<div id="mainTitle">
						<h4>Nueva encuesta</h4>
					</div>
					
					<div id="pasos">
						<a href="{$baseURL}/nueva-encuesta/paso1/" class="completadoLink"><span class="paso completado">1- Datos b&aacute;sicos</span></a>
						<span class="paso activo">2- Tus preguntas</span>
						<span class="paso pendiente">3- Configuraci&oacute;n</span>
					</div>
					
					<div class="clear"></div>
					
					<div id="formContainer">
					
						<p class="moduleTitle">A&ntilde;ade ahora las preguntas a tu encuesta. Podr&aacute;s seleccionar diferentes tipos de campos.</p>
						
						<div id="formContent">
							
								<a href="#" onclick="showNewField()"  class="addLink" onmouseover="showLinkLegend('addContainer')" onmouseout="clearLinkLegend('addContainer')"><span class="addButton">+</span></a>
								<p id="addContainer" class="addContainer">A&ntilde;ade un nuevo campo</p>
								
								<div id="newField">
									{include file="./privada/formCampos.tpl}									
								</div>
								
								<div id="formFields">
									campos del form								
								</div>
								<div id="botonera">
									<input type="button" value="Volver al Paso1" />
									<input type="button" value="Guardar cambios" />
									<input type="submit" value="Guardar e ir al Paso 3" />
								</div>
							
							
						</div>
					</div>
				</div>
			</div>		
			
			<!-- End Content -->

			<div class="orangeSlash"></div>
			
		<!-- Footer -->
			{include file='./includes/footer.tpl'}
		<!-- End Footer -->
			
		</div>		
	</body>
</html>