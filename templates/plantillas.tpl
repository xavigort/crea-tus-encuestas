﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head>
		{include file='./includes/head.tpl'}
	</head>
	<body>		
		<div id="container">
			
			<!-- Header -->			
			
				{include file='./includes/header.tpl'}
		
			<!-- End Header -->
			
			<!--[if lt IE 7]>
				{include file='./includes/ieFilter.tpl'}
			<![endif]-->
		
			<div class="orangeSlash"></div>
			
			<!-- Content -->
	
				<div id="topBody">					
					
						<div id="contentCol">
							<h4>Plantillas de Ejemplo</h4>
							<p class="text">11111Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
							
							<div class="blueLine"></div>
							
							<!-- Plantilla -->
							
								<div class="plantilla">
									
									<img src="{$baseURL}/images/plantillas/plantilla1.jpg" />
									
									<p class="plantillaTitle">Dolor sit amet 1</p>
									<p class="text">11111Lorem ips elitipsum dolor ipsum dolor sit ametipsum dolor sit ametipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
									
									<div class="iconos">
									
										<div class="icono">
											<img src="{$baseURL}/images/preview.jpg" />
											<p class="text"><a href="#">Previsualizar</a></p>
										</div>
										
										<div class="icono">
											<img src="{$baseURL}/images/usar.jpg" />
											<p class="text"><a href="#">Usar</a></p>
										</div>
										
										<div class="icono">
											<img src="{$baseURL}/images/modificar.jpg" />
											<p class="text"><a href="#">Modificar</a></p>
										</div>
									
									</div>
									
								</div>
							
							<!-- End Plantilla -->
							
							<div class="blueLine"></div>
							
							<!-- Plantilla -->
							
								<div class="plantilla">
									
									<img src="{$baseURL}/images/plantillas/plantilla2.jpg" />
									
									<p class="plantillaTitle">Dolor sit amet 2</p>
									<p class="text">11111Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do ei dolor sit amet, consectetur adipisicing elit, sed do ei dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
									
									<div class="iconos">

										<div class="icono">
											<img src="{$baseURL}/images/preview.jpg" />
											<p class="text"><a href="#">Previsualizar</a></p>
										</div>
										
										<div class="icono">
											<img src="{$baseURL}/images/usar.jpg" />
											<p class="text"><a href="#">Usar</a></p>
										</div>
										
										<div class="icono">
											<img src="{$baseURL}/images/modificar.jpg" />
											<p class="text"><a href="#">Modificar</a></p>
										</div>
									
									</div>
									
								</div>
							
							<!-- End Plantilla -->
							
							<div class="blueLine"></div>
							
							<!-- Plantilla -->
							
								<div class="plantilla">
									
									<img src="{$baseURL}/images/plantillas/plantilla3.jpg" />
									
									<p class="plantillaTitle">Dolor sit amet 3</p>
									<p class="text">11111Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
									
									<div class="iconos">
										
										<div class="icono">
											<img src="{$baseURL}/images/preview.jpg" />
											<p class="text"><a href="#">Previsualizar</a></p>
										</div>
										
										<div class="icono">
											<img src="{$baseURL}/images/usar.jpg" />
											<p class="text"><a href="#">Usar</a></p>
										</div>
										
										<div class="icono">
											<img src="{$baseURL}/images/modificar.jpg" />
											<p class="text"><a href="#">Modificar</a></p>
										</div>
									
									</div>
									
								</div>
							
							<!-- End Plantilla -->
							
							<div class="blueLine"></div>
							
							<!-- Plantilla -->
							
								<div class="plantilla">
									
									<img src="{$baseURL}/images/plantillas/plantilla4.jpg" />
									
									<p class="plantillaTitle">Dolor sit amet 4</p>
									<p class="text"><a href="#">1111Lorem ipsum </a>dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
									
									<div class="iconos">
									
										<div class="icono">
											<img src="{$baseURL}/images/preview.jpg" />
											<p class="text"><a href="#">Previsualizar</a></p>
										</div>
										
										<div class="icono">
											<img src="{$baseURL}/images/usar.jpg" />
											<p class="text"><a href="#">Usar</a></p>
										</div>
										
										<div class="icono">
											<img src="{$baseURL}/images/modificar.jpg" />
											<p class="text"><a href="#">Modificar</a></p>
										</div>
									
									</div>
									
								</div>
							
							<!-- End Plantilla -->
							
							<div class="blueLine"></div>
							
							<!-- Plantilla -->
							
								<div class="plantilla">
									
									<img src="{$baseURL}/images/plantillas/plantilla5.jpg" />
									
									<p class="plantillaTitle">Dolor sit amet 5</p>
									<p class="text">11111Lorem ipsum dolor sit ame consectetur adipisicing elit, sed do eius dolor sit ame consectetur adipisicing elit, sed do eius1111Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
									
									<div class="iconos">
									
										<div class="icono">
											<img src="{$baseURL}/images/preview.jpg" />
											<p class="text"><a href="#">Previsualizar</a></p>
										</div>
										
										<div class="icono">
											<img src="{$baseURL}/images/usar.jpg" />
											<p class="text"><a href="#">Usar</a></p>
										</div>
										
										<div class="icono">
											<img src="{$baseURL}/images/modificar.jpg" />
											<p class="text"><a href="#">Modificar</a></p>
										</div>
									
									</div>
									
								</div>

								<!-- End Plantilla -->
								
								<div class="blueLine"></div>
								
								<div id="pagination">
								
									<ul id="paginationList">
										<li><a href="#">Anterior</a></li>
										<li class="spacer">-</li>
										<li><a href="#">1</a></li>
										<li class="spacer">-</li>
										<li>...</li>
										<li class="spacer">-</li>
										<li><a href="#">4</a></li>
										<li class="spacer">-</li>
										<li class="active">5</li>
										<li class="spacer">-</li>
										<li><a href="#">6</a></li>
										<li class="spacer">-</li>
										<li>...</li>				
										<li class="spacer">-</li>
										<li><a href="#">16</a></li>				
										<li class="spacer">-</li>
										<li><a href="#">Siguiente</a></li>										
									</ul>
								</div>
							
							
						</div>

						<div class="spacerW10">&nbsp;</div>
						
						<div id="modulesCol">
							
						<!-- Destacado 1 -->
						
							<div id="moduloDestacado1">
							
								<p id="modulo1Title">Ejemplo Lateral 1</p>
								<p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>					
								
								<ul id="modulo1Listado">
									<li>Ut enim ad minim veniamorem</li>
									<li>Lorem ipsum dolor Ut enim ad minim veniamorem</li>
									<li>Lorem Ut enim ad minim veniamoremdolor</li>
									<li>Lorem ipsum dolor ipsum dolor</li>
									<li>Lorem ipsum doloripsum doloripsum dolor</li>
									<li>Lorem Ut enim ad </li>
								</ul>
								
								
								<div id="registerButton">
									<a class="registerLink" href="#"><span class="registerButton">REG&Iacute;STRATE</span></a>
									
									<!-- Gratis -->
									<img id="gratisButton" src="{$baseURL}/images/gratis.png" />
									<!-- End Gratis -->
								</div>
								
							</div>
						
						<!-- End Destacado 1 -->
						
						<!-- Destacado 2 -->
						
							<div id="moduloDestacado2">
							
								<p id="modulo2Title">Ejemplo Lateral 1</p>
								<p class="text">"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
								<p class="text">"Lorem ipsum dolor sit amiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
								<p class="text">"Lorem ipsum dolor sit am labore et dolore magna aliqua."</p>
								<p class="text">"Lorem ipsum dolor sit amet, consectetur adipisicing elit,et, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>							
								
							</div>
						
						<!-- End Destacado 2 -->
						
						
						
						</div>
					
							
				</div>				
				
				
			<!-- End Content -->

			<div class="orangeSlash"></div>
			
		<!-- Footer -->
			
			{include file='./includes/footer.tpl'}
		
		<!-- End Footer -->
			
		</div>		
		
	</body>
</html>