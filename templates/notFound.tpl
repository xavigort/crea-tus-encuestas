﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head>
		{include file='./includes/head.tpl'}
	</head>
	<body>		
		<div id="container">
			
			<!-- Header -->			
			
				{include file='./includes/header.tpl'}
		
			<!-- End Header -->
			
			<!--[if lt IE 7]>
				{include file='./includes/ieFilter.tpl'}
			<![endif]-->
		
			<div class="orangeSlash"></div>
			
			<!-- Content -->
	
				<div id="topBody">
					
						<div id="fullContent">
						
							<h4>P&aacute;gina no encontrada</h4>
							<p class="text">&nbsp;</p>
							<p class="text">La p&aacute;gina a la que intentas acceder no existe. Int&eacute;ntalo de nuevo, o bien busca entre las siguientes opciones la que m&aacute;s se adapte a tu caso:</p>
							<ul id="modulo3Listado">
								<li>Me he perdido, querr&iacute;a <a href="{$baseURL}">volver a la p&aacute;gina de inicio</a>.</li>
								<li>Tengo claro lo que estoy buscando pero no lo encuentro. Tal vez deber&iacute;a <a href="{$baseURL}/faqs">consultar las faq's</a>.</li>
								<li>El contenido que buscaba ya no existe, me gustar&iacute;a <a href="{$baseURL}/contacto">ponerme en contacto</a> con el equipo de Crea Tus Encuestas.</li>
							</ul>
							
						</div>				
							
				</div>								
				
			<!-- End Content -->

			<div class="orangeSlash"></div>
			
		<!-- Footer -->
			
			{include file='./includes/footer.tpl'}
		
		<!-- End Footer -->
			
		</div>		
		
	</body>
</html>