﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head>		
		{include file='./privada/headPrivada.tpl'}
		<!-- jqtranform - Nice Javascript Forms -->
		
		<link rel="stylesheet" type="text/css" href="{$baseURL}/js/jqtransformplugin/jqtransform.css" media="screen" />
		<script src="{$baseURL}/js/jqtransformplugin/jquery.jqtransform.js" type="text/javascript"></script>
		
		{literal}
		
			<script type="text/javascript">
				$(function(){
					$('form').jqTransform({imgPath:'{$baseURL}/js/jqtransformplugin/img/'});
				});
				
			</script>
		
		{/literal}
		
		<!-- Nice Javascript Form validator -->
		
		<link rel="stylesheet" type="text/css" href="{$baseURL}/js/jqValidation/validationEngine.jquery.css" media="screen" />
		<script src="{$baseURL}/js/jqValidation/jquery.validationEngine-en.js" type="text/javascript"></script>
		<script src="{$baseURL}/js/jqValidation/jquery.validationEngine.js" type="text/javascript"></script>
		
		{literal}
			<script type="text/javascript">
				$(document).ready(function() 
				{
					$("#form1").validationEngine()
				});
			</script>	
		{/literal}	

	</head>
	<body>
		<div id="container">
			
			<!-- Header -->
				{include file='./privada/headerPrivada.tpl'}
		
			<!-- End Header -->
			
			<div class="orangeSlash"></div>
			
			<!-- Content -->
	
			<div id="content">
				
				<!-- Modules Col -->
					{include file="./privada/menuPrivada.tpl"}
				<!-- End Modules Col -->
				
				<!-- ContentCol -->
				
				<div id="mainContent">
					
					<div id="mainTitle">
						<h4>Nueva encuesta</h4>
					</div>
					
					<div id="pasos">
						<a href="{$baseURL}/nueva-encuesta/paso1/" class="completadoLink"><span class="paso completado">1- Datos b&aacute;sicos</span></a>
						<a href="{$baseURL}/nueva-encuesta/paso2/" class="completadoLink"><span class="paso completado">2- Tus preguntas</span></a>
						<span class="paso activo">3- Configuraci&oacute;n</span>
					</div>
					
					<div class="clear"></div>
					
					<div id="formContainer">
					
						<p class="moduleTitle">Para finalizar tu encuesta, indica los par&aacute;metros de configuración.</p>
						
						<div id="formContent">
							
							<form id="form1" action="{$baseURL}/nueva-encuesta/completada/" method="post">							
							
								<div class="rowElem"><label>&iquest;Deseas control de acceso <strong>con contrase&ntilde;a</strong>? <span class="fieldExplain" onmouseover="tooltip.show('<strong>lorem ipsum.</strong> dolor sit amet');" onmouseout="tooltip.hide();">?</span></label>
									<input type="radio" onClick="showHiddenField('surveyPasswordContainer')" id="surveyPassword1" name="surveyPassword" value="1" /><label class="uniLineal">Si</label>
									<input type="radio" onClick="hideVisibleField('surveyPasswordContainer')" id="surveyPassword0" name="surveyPassword" value="0" checked /><label class="uniLineal">No</label>
								</div>
								<div class="vHidden dNone" id="surveyPasswordContainer">
									<div class="rowElem"><label>* Indica la <strong>contrase&ntilde;a</strong> para tu encuesta <span class="fieldExplain" onmouseover="tooltip.show('<strong>lorem ipsum.</strong> dolor sit amet');" onmouseout="tooltip.hide();">?</span></label><input type="text" maxlength="15" id="surveyPassword" name="surveyPassword" value="creaencuestas" class="validate[required,length[8,15]]" /></div>
								</div>
								<div class="rowElem"><label>&iquest;C&oacute;mo quieres <strong>identificar a los usuarios</strong> que rellenen tu encuesta? <span class="fieldExplain" onmouseover="tooltip.show('<strong>lorem ipsum.</strong> dolor sit amet');" onmouseout="tooltip.hide();">?</span></label>
									<input type="radio" onClick="hideVisibleField('identifyFieldContainer'); hideVisibleField('uniqueResponseContainer')" id="identifyUser0" name="identifyUser" value="0" checked /><label class="uniLineal">No identificar</label>
									<input type="radio" onClick="hideVisibleField('identifyFieldContainer'); showHiddenField('uniqueResponseContainer')" id="identifyUser1" name="identifyUser" value="1" /><label class="uniLineal">Con su email</label>
									<input type="radio" onClick="hideVisibleField('identifyFieldContainer'); showHiddenField('uniqueResponseContainer')" id="identifyUser2" name="identifyUser" value="2" /><label class="uniLineal">Con nombre y apellidos</label>
									<input type="radio" onClick="showHiddenField('identifyFieldContainer'); showHiddenField('uniqueResponseContainer')" id="identifyUser3" name="identifyUser" value="3" /><label class="uniLineal">Con otro campo</label>
								</div>
								<div class="vHidden dNone" id="identifyFieldContainer">
									<div class="rowElem"><label>* Indica el texto con que quieres que <strong>se identifiquen</strong> tus usuarios <span class="fieldExplain" onmouseover="tooltip.show('<strong>lorem ipsum.</strong> dolor sit amet');" onmouseout="tooltip.hide();">?</span></label><input type="text" id="identifyField" name="identifyField" value="Escribe tu DNI" class="validate[required,length[0,150]]" /></div>
								</div>
								<div class="vHidden dNone" id="uniqueResponseContainer">
									<div class="rowElem"><label>&iquest;Deseas evitar que se repita el mismo identificador de usuario <strong>varias veces</strong>?<span class="fieldExplain" onmouseover="tooltip.show('<strong>lorem ipsum.</strong> dolor sit amet');" onmouseout="tooltip.hide();">?</span></label>
										<input type="radio" id="uniqueResponse0" name="uniqueResponse" value="0" /><label class="uniLineal">Si</label>
										<input type="radio" id="uniqueResponse1" name="uniqueResponse" value="1" checked /><label class="uniLineal">No</label>									
									</div>
								</div>
								<div class="rowElem"><label>&iquest;Quieres que los usuarios <strong>vean los resultados</strong> una vez hayan rellenado la encuesta?<span class="fieldExplain" onmouseover="tooltip.show('<strong>estadisticas </strong> solamente veran resultados de los campos de desplegable, selector unico y simple, para el resto de campos (texto y parrafos) veran sus respuestas');" onmouseout="tooltip.hide();">?</span></label>
									<input type="radio" id="userSeeResults1" name="userSeeResults" value="1" /><label class="uniLineal">Si</label>
									<input type="radio" id="userSeeResults0" name="userSeeResults" value="0" checked /><label class="uniLineal">No</label>									
								</div>								
								<div class="rowElem"><label>&iquest;Te gustar&iacute;a <strong>recibir un correo</strong> cada vez que algun usuario responda tu encuesta?<span class="fieldExplain" onmouseover="tooltip.show('<strong>lorem ipsum.</strong> dolor sit amet');" onmouseout="tooltip.hide();">?</span></label>
									<input type="radio" id="receiveEmail1" name="receiveEmail" value="1" /><label class="uniLineal">Si</label>
									<input type="radio" id="receiveEmail0" name="receiveEmail" value="0" checked /><label class="uniLineal">No</label>									
								</div>
								<div class="rowElem"><label>&iquest;Quieres que tu encuesta apareza en la secci&oacute; <strong>&uacute;ltimas encuestas</strong> de la web?<span class="fieldExplain" onmouseover="tooltip.show('<strong>lorem ipsum.</strong> dolor sit amet');" onmouseout="tooltip.hide();">?</span></label>
									<input type="radio" id="lastSurveys1" name="lastSurveys" value="1" /><label class="uniLineal">Si</label>
									<input type="radio" id="lastSurveys0" name="lastSurveys" value="0" checked /><label class="uniLineal">No</label>									
								</div>
								<div class="rowElem">
									<label>Estilo de tu encuesta:</label>
									<select id="surveyStyle" name="surveyStyle" onChange="showThumbnail(this.value);">
										<option value="0">Elige tu estilo</option>
										<option value="1">Personalizado</option>
										<option value="2">Moderno</option>
										<option value="3">Ejecutivo</option>
										<option value="4">Informal</option>
										<option value="5">Celebraci&oacute;n</option>
										<!--<option value="6">Familiar</option>-->
									</select>
									<div id="stylePreview">
										<div id="styleThumb1" class="styleThumbPersonaliza">
											<div id="thumbPersonalizaLeft">
												<label for="color1">color1<input name="color1" class="jqtranformdone selectorColor" id="color1" type="text" onclick="startColorPicker(this)" onkeyup="maskedHex(this)" /></label>
												<label for="color2">color2<input name="color2" class="jqtranformdone selectorColor" id="color2" type="text" onclick="startColorPicker(this)" onkeyup="maskedHex(this)" /></label>
												<label for="color3">color3<input name="color3" class="jqtranformdone selectorColor" id="color3" type="text" onclick="startColorPicker(this)" onkeyup="maskedHex(this)" /></label>
												<label for="color4">color4<input name="color4" class="jqtranformdone selectorColor" id="color4" type="text" onclick="startColorPicker(this)" onkeyup="maskedHex(this)" /></label>
												<label for="color5">color5<input name="color5" class="jqtranformdone selectorColor" id="color5" type="text" onclick="startColorPicker(this)" onkeyup="maskedHex(this)" /></label>
												<label for="color6">color6<input name="color6" class="jqtranformdone selectorColor" id="color6" type="text" onclick="startColorPicker(this)" onkeyup="maskedHex(this)" /></label>
												
											</div>
											<div id="thumbPersonalizaRight">
												<div id="fonsThumbPersonal">
													<div id="fullThumbPersonal">
														<div id="gridThumbPersonal">
															AA
														</div>		
													</div>													
												</div>
											</div>
											
										</div>
										<img id="styleThumb2" src="{$baseURL}/images/plantillas/plantilla2.jpg" class="styleThumb dNone vHidden" />
										<img id="styleThumb3" src="{$baseURL}/images/plantillas/plantilla3.jpg" class="styleThumb dNone vHidden" />
										<img id="styleThumb4" src="{$baseURL}/images/plantillas/plantilla4.jpg" class="styleThumb dNone vHidden" />
										<img id="styleThumb5" src="{$baseURL}/images/plantillas/plantilla5.jpg" class="styleThumb dNone vHidden" />
									</div>
								</div>
								<div id="botonera">
									<input type="button" onClick="alert('ueeee');" value="Volver al Paso2" />
									<input type="submit" value="Guardar cambios" />
								</div>
							</form>
														
						</div>
					</div>
				</div>
			</div>		
			
			<!-- End Content -->

			<div class="orangeSlash"></div>
			
		<!-- Footer -->
			{include file='./includes/footer.tpl'}
		<!-- End Footer -->
			
		</div>		
	</body>
</html>