﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head>
		{include file='./includes/head.tpl'}
	</head>
	<body>		
		<div id="container">
			
			<!-- Header -->			
			
				{include file='./includes/header.tpl'}
		
			<!-- End Header -->
			
			<!--[if lt IE 7]>
				{include file='./includes/ieFilter.tpl'}
			<![endif]-->
		
			<div class="orangeSlash"></div>
			
			<!-- Content -->
	
				<div id="topBody">
					
					<!-- Left Col -->
					
						<div id="modulesCol">
							
						<!-- Destacado 1 -->
						
							<div id="moduloDestacado3">
							
								<p id="modulo3Title">Menu FAQs</p>
								<p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>					
								
								<ul id="modulo3Listado">
									<li><a href="#">Ut enim ad minim veniamorem</a></li>
									<li>Lorem ipsum dolor Ut enim ad minim veniamorem</li>
									<li>Lorem Ut <a href="#">enim ad minim </a>veniamoremdolor</li>
									<li>Lorem ipsum dolor ipsum dolor</li>
									<li>Lorem ipsum doloripsum doloripsum dolor</li>
									<li>Lorem Ut enim ad </li>
								</ul>
								
							</div>
						
						<!-- End Destacado 1 -->																
						
						</div>
					
					<!-- End Left Col -->
					
					<div class="spacerW10">&nbsp;</div>
					
					<!-- Right Col -->
					
						<div id="contentCol">
						
							<h4>FAQs</h4>
							<p class="text">11111Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
							<p class="text">Sed ut perspiciatis unde omnis iste natus error sit <a href="#">voluptatem accusantium</a> doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciun</p>
							
							<div class="textCol">
								<p class="plantillaTitle">Dolor sit amet 5</p>
								<p class="text">11111Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								<p class="text">Ut enim ad minima veniam, quis nostrum exercitnem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?</p>
								<img src="{$baseURL}/images/faqs1" alt="faqs1" />
								<p class="text">Ut enim ad minima veniam, quis nostrum exercitnem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?</p>
							</div>
							
							<div class="textCol">
								<p class="plantillaTitle">Dolor sit amet 5</p>
								<p class="text">Lorem ipsum dolort sit amet, consectetur adipisicing elit, sed do eiusmod temput labore et dolore magna aliqua.</p>
								<p class="text">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
								<p class="text">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit.</p>
								<p class="text">Quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.</p>
							</div>
							
						</div>
					
					<!-- End Right Col -->						
							
				</div>								
				
			<!-- End Content -->

			<div class="orangeSlash"></div>
			
		<!-- Footer -->
			
			{include file='./includes/footer.tpl'}
		
		<!-- End Footer -->
			
		</div>		
		
	</body>
</html>