﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head>
		{include file='./includes/head.tpl'}
	</head>
	<body>		
		<div id="container">
			
			<!-- Header -->			
			
				{include file='./includes/header.tpl'}
				<!-- SLIDES-->
				<script src="{$baseURL}/js/jquery.cycle.all.2.72.js" type="text/javascript"></script>		
		
				<!-- Específicas-->
				<script src="{$baseURL}/js/library.js" type="text/javascript"></script>
				<script src="{$baseURL}/js/functions.js" type="text/javascript"></script>
				
				{literal}	
					<script type="text/javascript">
					$(document).ready(function() {
							$('.slideshow').cycle({
							fx: 'fade' // choose your transition type, ex: fade, scrollUp, shuffle, etc...
						});
					});
					</script>
				{/literal}
		
		
			<!-- End Header -->
			
			<!--[if lt IE 7]>
				{include file='./includes/ieFilter.tpl'}
			<![endif]-->
		
			<!-- Content -->

				<div class="orangeSlash"></div>
				
				<!-- First Block -->
				
					<div id="topBody">
						
						<!-- Claims -->
							<div id="claim">
								<h1>Crea tus encuestas</h1>
								<h2>en 3 sencillos pasos... y que opinen!</h2>
								
									<!-- Claim -->
									<div class="moduleClaim">
										<div class="logoClaim"><img src="{$baseURL}/images/claim1.jpg" /></div>
										<h3 class="claimTitle">Crear</h3>
										<p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
									</div>
									<!-- End Claim -->
									<!-- Claim -->
									<div class="moduleClaim">
										<div class="logoClaim"><img src="{$baseURL}/images/claim2.jpg" /></div>
										<h3 class="claimTitle">Dise&ntilde;ar</h3>
										<p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
									</div>
									<!-- End Claim -->
									<!-- Claim -->
									<div class="moduleClaim">
										<div class="logoClaim"><img src="{$baseURL}/images/claim3.jpg" /></div>
										<h3 class="claimTitle">Difundir</h3>
										<p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
									</div>
									<!-- End Claim -->
								
							</div>
						<!-- End Claims -->
						
						<!-- Highlights -->
						
							<div id="highlights">
								<div class="slideshow">
									<img src="{$baseURL}/images/slides/slide1.png" />
									<img src="{$baseURL}/images/slides/slide2.png" />
									<img src="{$baseURL}/images/slides/slide3.png" />
								</div>
								<div id="registerButtons">
									<a class="registerLinks" href="#"><span class="registerButtons">PRU&Eacute;BALO</span></a>
									<a class="registerLinks" href="#"><span class="registerButtons">REG&Iacute;STRATE</span></a>
									
									<!-- Gratis -->
									<img id="gratis" src="{$baseURL}/images/gratis.png" />
									<!-- End Gratis -->
								</div>
								
							</div>
						
						<!-- End Highlights -->
					
					</div>
				
				<!-- End First Block -->
				
				<div class="orangeSlash"></div>
				
				<!-- Second Block -->
				
					<div id="bottomBody">
						
						<!-- Properties -->
						
							<div id="caracteristicas">
							
								<div id="caracteristicasLeft">
									
									<h4>Caracter&iacute;sticas</h4>
									
									<div id="caracteristicasContainer">
										<div id="caracteristica1" class="">
											<img id="thumbnailDestacado1" src="{$baseURL}/images/thumbnails/thumbnail1.jpg" alt="lorem ipsum" />
											<p class="thumbnailTitle">Dolor sit amet 1</p>
											<p class="text">11111Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
										</div>

										<div id="caracteristica2" class="caracteristica">
											<img id="thumbnailDestacado2" src="{$baseURL}/images/thumbnails/thumbnail2.jpg" alt="lorem ipsum" />
											<p class="thumbnailTitle">Lorem ipsum dolor si 2</p>
											<p class="text">22222Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
										</div>
										<div id="caracteristica3" class="caracteristica">
											<img id="thumbnailDestacado3" src="{$baseURL}/images/thumbnails/thumbnail3.jpg" alt="lorem ipsum" />
											<p class="thumbnailTitle">Lorem ipsum dolor si 3333</p>
											<p class="text">3333Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
										</div>
										<div id="caracteristica4" class="caracteristica">
											<img id="thumbnailDestacado4" src="{$baseURL}/images/thumbnails/thumbnail4.jpg" alt="lorem ipsum" />
											<p class="thumbnailTitle">Lorem ipsum dolor si 4444</p>
											<p class="text">4444Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
										</div>
									</div>
								</div>
								
								<div id="caracteristicasRight">
									<ul id="caracteristicasListado">
										<li><a class="active" id="item1" href="#" onClick="return showCaracteristica('1');">Dolor sit amet 1</a></li>
										<li><a href="#" id="item2" onClick="return showCaracteristica('2');">Lorem ipsum dolor si2</a></li>
										<li><a href="#" id="item3" onClick="return showCaracteristica('3');">Lorem ipsum dolor si3</a></li>
										<li><a href="#" id="item4" onClick="return showCaracteristica('4');">Lorem ipsum dolor si4</a></li>											
									</ul>
								</div>					
							</div>
						
						<!-- End Properties -->
						
						<!-- Premium -->
						
							<div id="ventajas">
								<p id="titleVentajas">Ventajas premium</p>
								<p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>					
								<ul id="ventajasListado">
									<li>Ut enim ad minim veniamorem</li>
									<li>Lorem ipsum dolor Ut enim ad minim veniamorem</li>
									<li>Lorem Ut enim ad minim veniamoremdolor</li>
									<li>Lorem ipsum dolor ipsum dolor</li>
									<li>Lorem ipsum doloripsum doloripsum dolor</li>
									<li>Lorem Ut enim ad </li>
								</ul>
							</div>
						
						
						
						<!-- End Premium -->
						
					</div>				
				
				<!-- End Second Block -->
				
			<!-- End Content -->

			<div class="orangeSlash"></div>
			
		<!-- Footer -->
			
			{include file='./includes/footer.tpl'}
		
		<!-- End Footer -->
			
		</div>		
		
	</body>
</html>