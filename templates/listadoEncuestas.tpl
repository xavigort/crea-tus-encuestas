﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head>		
		{include file='./privada/headPrivada.tpl'}		
		{literal}
			<script type="text/javascript">
				function mainmenu(){
				$(" #nav ul ").css({display: "none"});
				$(" #nav li").hover(function(){
					$(this).find('ul:first:hidden').css({visibility: "visible",display: "none"}).slideDown(400);
					},function(){
						$(this).find('ul:first').slideUp(400);
					});
				}
				$(document).ready(function(){
					mainmenu();
				});
			</script>
		{/literal}
	</head>
	<body>		
		<div id="container">
			
			<!-- Header -->						
				{include file='./privada/headerPrivada.tpl'}		
			<!-- End Header -->
			
			<div class="orangeSlash"></div>
			
			<!-- Content -->
	
				<div id="content">
					
				<!-- Modules Col -->					
						{include file="./privada/menuPrivada.tpl"}					
					<!-- End Modules Col -->
					
					<!-- ContentCol -->
					
					<div id="mainContent">
						
						<div id="mainTitle">
							<h4>Encuestas</h4>
							<div id="leyenda">
								<p id="leyendaText">Visualiza tus encuestas</p>
								<a href="#" id="eBorrador"><span id="bgBorrador">Borradores (3)</span></a>
								<a href="#" id="ePublicada"><span id="bgPublicada">Publicadas (1)</span></a>
								<a href="#" id="eCerrada"><span id="bgCerrada">Cerradas (2)</span></a>
							</div>
						</div>
						<div id="displayContent">
							
							<!-- Top Display -->
							
								<div id="topDisplay">
								
									<div class="addLinkContainer">
										<a class="addLink" href="{$baseURL}/nueva-encuesta/paso1/" onmouseover="showLinkLegend('addContainer')" onmouseout="clearLinkLegend('addContainer')"><span class="addButton">+</span></a>
										<p id="addContainer" class="addContainer">A&ntilde;ade una nueva encuesta</p>
									</div>
									
									<div class="paginationContainer">
										<ul class="pagination">
											<li><a title="Anterior" href="#"><span class="leftArrow">&nbsp;</span></a></li>
											<li><a href="#">1</a></li>
											<li>...</li>
											<li><a href="#">6</a></li>
											<li><a href="#">7</a></li>
											<li class="active">8</li>
											<li><a href="#">9</a></li>
											<li><a href="#">10</a></li>
											<li>...</li>
											<li><a href="#">27</a></li>
											<li><a title="Siguiente" href="#" class="linkRightArrow"><span class="rightArrow">&nbsp;</span></a></li>
										</ul>
									</div>
									
									<div id="optionsContainer">
										<div class="clear"></div>
										<ul id="nav">
											<li>
												<a class="orderTitle" href="#">Ordenar por</a>
												<ul class="submenu">
													<li>
														<a href="#">Fecha Creaci&oacute;n</a>
														<ul class="subsubmenu">
															<li><a href="#">La m&aacute;s reciente primero</a></li>
															<li><a href="#">La m&aacute;s antigua primero</a></li>
														</ul>
													</li>
													<li>
														<a href="#">Nombre</a>
														<ul class="subsubmenu">
															<li><a href="#">Ascendente (A-Z)</a></li>
															<li><a href="#">Descendenete (Z-A)</a></li>
														</ul>
													</li>
													<li>
														<a href="#">N&ordm; respuestas</a>
														<ul class="subsubmenu">
															<li><a href="#">De m&aacute;s a menos</a></li>
															<li><a href="#">De menos a m&aacute;s</a></li>
														</ul>
													</li>													
												</ul>
											 </li>
										</ul>
									</div>
								</div>
							
							<!-- End Top Display -->
							
							<div id="infoDisplay">
							
								<!-- Encuesta Module -->
								
									<div class="moduloEncuesta publicada">
										
										<div class="titleEncuesta">
											<p class="title">Satisfacción de Cliente</p>
											<p class="titleInfo">27 Respuestas - (23-01-2010)</p>
											<div class="clear"></div>
											<p class="titleAction" id="titleAction_1">&nbsp;</p>
										</div>
										
										
										
										<div class="contentEncuesta">	
											
											<div class="descriptionEncuesta">
												Lorem ipsum dolor sit amet, consectetur adipisisit amet, consectetur adipisisit amet, consectetur adipisicing elit,Lorem i
											</div>
											
											<div class="iconasEncuesta">
												<a href="#" onMouseOver="showLegend(1,1)" onMouseOut="clearLegend(1);"><span class="viewIcon">&nbsp;</span></a>
												<a href="#" onMouseOver="showLegend(1,2)" onMouseOut="clearLegend(1);"><span class="editIcon">&nbsp;</span></a>
												<a href="#" onMouseOver="showLegend(1,4)" onMouseOut="clearLegend(1);"><span class="unPublishIcon">&nbsp;</span></a>
												<a href="#" onMouseOver="showLegend(1,5)" onMouseOut="clearLegend(1);"><span class="spreadIcon">&nbsp;</span></a>
												<a href="#" onMouseOver="showLegend(1,6)" onMouseOut="clearLegend(1);"><span class="statsIcon">&nbsp;</span></a>
												<span class="deleteIconInactive" onMouseOver="showLegend(1,700)" onMouseOut="clearLegend(1);">&nbsp;</span>
											</div>
											<div class="clear"></div>
											
										</div>


									</div>
									
								<!-- End Encuesta Module -->
								
								<!-- Encuesta Module -->
								
									<div class="moduloEncuesta cerrada">
										
										<div class="titleEncuesta">
											<p class="title">Satisfacción de Cliente</p>
											<p class="titleInfo">27 Respuestas - (23-01-2010)</p>
											<div class="clear"></div>
											<p class="titleAction" id="titleAction_2">&nbsp;</p>
										</div>
										
										
										<div class="contentEncuesta">	
											
											<div class="descriptionEncuesta">
												Lorem ipsum dolor sit amet, consectetur adipisicing elit,Lorem i
											</div>
											
											<div class="iconasEncuesta">
												<a href="#" onMouseOver="showLegend(2,1)" onMouseOut="clearLegend(2);"><span class="viewIcon">&nbsp;</span></a>
												<a href="#" onMouseOver="showLegend(2,2)" onMouseOut="clearLegend(2);"><span class="editIcon">&nbsp;</span></a>
												<a href="#" onMouseOver="showLegend(2,3)" onMouseOut="clearLegend(2);"><span class="publishIcon">&nbsp;</span></a>
												<span class="spreadIconInactive" onMouseOver="showLegend(2,500)" onMouseOut="clearLegend(2);">&nbsp;</span>
												<a href="#" onMouseOver="showLegend(2,6)" onMouseOut="clearLegend(2);"><span class="statsIcon">&nbsp;</span></a>
												<a href="#" onMouseOver="showLegend(2,7)" onMouseOut="clearLegend(2);"><span class="deleteIcon">&nbsp;</span></a>												
											</div>
											<div class="clear"></div>
											
										</div>


									</div>
									
								<!-- End Encuesta Module -->
								
								<!-- Encuesta Module -->
								
									<div class="moduloEncuesta borrador">
										
										<div class="titleEncuesta">
											<p class="title">Satisfacción de Cliente</p>
											<p class="titleInfo">27 Respuestas - (23-01-2010)</p>
											<div class="clear"></div>
											<p class="titleAction" id="titleAction_23">&nbsp;</p>
										</div>
										
										
										<div class="contentEncuesta">	
											
											<div class="descriptionEncuesta">
												Lorem ipsum dolor sit amet, consectetur adipisicing elit,Lorem i
											</div>
											
											<div class="iconasEncuesta">
												<a href="#" onMouseOver="showLegend(23,1)" onMouseOut="clearLegend(23);"><span class="viewIcon">&nbsp;</span></a>
												<a href="#" onMouseOver="showLegend(23,2)" onMouseOut="clearLegend(23);"><span class="editIcon">&nbsp;</span></a>
												<a href="#" onMouseOver="showLegend(23,3)" onMouseOut="clearLegend(23);"><span class="publishIcon">&nbsp;</span></a>
												<span class="spreadIconInactive" onMouseOver="showLegend(23,500)" onMouseOut="clearLegend(23);">&nbsp;</span>
												<span class="statsIconInactive" onMouseOver="showLegend(23,600)" onMouseOut="clearLegend(23);">&nbsp;</span>
												<a href="#" onMouseOver="showLegend(23,7)" onMouseOut="clearLegend(23);"><span class="deleteIcon">&nbsp;</span></a>												
											</div>
											<div class="clear"></div>
											
										</div>


									</div>
									
								<!-- End Encuesta Module -->
								
								<!-- Encuesta Module -->
								
									<div class="moduloEncuesta borrador">
										
										<div class="titleEncuesta">
											<p class="title">Satisfacción de Cliente</p>
											<p class="titleInfo">27 Respuestas - (23-01-2010)</p>
											<div class="clear"></div>
											<p class="titleAction" id="titleAction_15">&nbsp;</p>
										</div>
										
										
										<div class="contentEncuesta">	
											
											<div class="descriptionEncuesta">
												Lorem ipsum dolor sit amet, consectetur adipisicing elit,Lorem i
											</div>
											
											<div class="iconasEncuesta">
												<a href="#" onMouseOver="showLegend(15,1)" onMouseOut="clearLegend(15);"><span class="viewIcon">&nbsp;</span></a>
												<a href="#" onMouseOver="showLegend(15,2)" onMouseOut="clearLegend(15);"><span class="editIcon">&nbsp;</span></a>
												<a href="#" onMouseOver="showLegend(15,3)" onMouseOut="clearLegend(15);"><span class="publishIcon">&nbsp;</span></a>
												<span class="spreadIconInactive" onMouseOver="showLegend(15,500)" onMouseOut="clearLegend(15);">&nbsp;</span>
												<span class="statsIconInactive" onMouseOver="showLegend(15,600)" onMouseOut="clearLegend(15);">&nbsp;</span>
												<a href="#" onMouseOver="showLegend(15,7)" onMouseOut="clearLegend(15);"><span class="deleteIcon">&nbsp;</span></a>												
											</div>
											<div class="clear"></div>
											
										</div>


									</div>
									
								<!-- End Encuesta Module -->
								
								<!-- Encuesta Module -->
								
									<div class="moduloEncuesta cerrada">
										
										<div class="titleEncuesta">
											<p class="title">Satisfacción de Cliente</p>
											<p class="titleInfo">27 Respuestas - (23-01-2010)</p>
											<div class="clear"></div>
											<p class="titleAction" id="titleAction_21">&nbsp;</p>
										</div>
										
										
										<div class="contentEncuesta">	
											
											<div class="descriptionEncuesta">
												Lorem ipsum dolor sit amet, consectetur adipisicing elit,Lorem i
											</div>
											
											<div class="iconasEncuesta">
												<a href="#" onMouseOver="showLegend(21,1)" onMouseOut="clearLegend(21);"><span class="viewIcon">&nbsp;</span></a>
												<a href="#" onMouseOver="showLegend(21,2)" onMouseOut="clearLegend(21);"><span class="editIcon">&nbsp;</span></a>
												<a href="#" onMouseOver="showLegend(21,3)" onMouseOut="clearLegend(21);"><span class="publishIcon">&nbsp;</span></a>
												<span class="spreadIconInactive" onMouseOver="showLegend(21,500)" onMouseOut="clearLegend(21);">&nbsp;</span>
												<a href="#" onMouseOver="showLegend(21,6)" onMouseOut="clearLegend(21);"><span class="statsIcon">&nbsp;</span></a>
												<a href="#" onMouseOver="showLegend(21,7)" onMouseOut="clearLegend(21);"><span class="deleteIcon">&nbsp;</span></a>												
											</div>
											<div class="clear"></div>
											
										</div>


									</div>
									
								<!-- End Encuesta Module -->
								
							</div>
							
							<!-- Bottom Display -->					
					
								<div id="bottomDisplay">
									<div class="addLinkContainer">
										<a class="addLink" href="{$baseURL}/nueva-encuesta/paso1/" onmouseover="showLinkLegend('addContainer2')" onmouseout="clearLinkLegend('addContainer2')"><span class="addButton">+</span></a>
										<p id="addContainer2" class="addContainer">A&ntilde;ade una nueva encuesta</p>
									</div>
									
									<div class="paginationContainer">
										<ul class="pagination">
											<li><a title="Anterior" href="#"><span class="leftArrow">&nbsp;</span></a></li>
											<li><a href="#">1</a></li>
											<li>...</li>
											<li><a href="#">6</a></li>
											<li><a href="#">7</a></li>
											<li class="active">8</li>
											<li><a href="#">9</a></li>
											<li><a href="#">10</a></li>
											<li>...</li>
											<li><a href="#">27</a></li>
											<li><a title="Siguiente" href="#" class="linkRightArrow"><span class="rightArrow">&nbsp;</span></a></li>
										</ul>
									</div>
									<!--
									<div class="deleteLinkContainer">
										<a class="deleteLink" href="#" onmouseover="showLinkLegend('deleteContainer')" onmouseout="clearLinkLegend('deleteContainer')"><span class="deleteButton">-</span></a>
										<p id="deleteContainer">Elimina encuestas seleccionadas</p>
									</div>
									-->
									
								</div>
							
							<!-- End Bottom Display -->
							
						</div>
						
					</div>
							
				</div>								
				
			<!-- End Content -->

			<div class="orangeSlash"></div>
			
		<!-- Footer -->			
			{include file='./includes/footer.tpl'}			
		<!-- End Footer -->
			
		</div>		
		
	</body>
</html>