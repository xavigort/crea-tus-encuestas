﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head>		
		{include file='./includes/headPrivada.tpl'}		
		
		
		<!-- jqtranform - Nice Javascript Forms --> 
		
		<link rel="stylesheet" type="text/css" href="{$baseURL}/js/jqtransformplugin/jqtransform.css" media="screen" />				
		<script src="{$baseURL}/js/jqtransformplugin/jquery.jqtransform.js" type="text/javascript"></script>
		
		{literal}
		
			<script type="text/javascript" language="javascript">
				$(function(){
					$('form').jqTransform({imgPath:'{$baseURL}/js/jqtransformplugin/img/'});
				});				
				
			</script>
		
		{/literal}	
		
		<!-- Nice Javascript Form validator --> 
		
		<link rel="stylesheet" type="text/css" href="{$baseURL}/js/jqValidation/validationEngine.jquery.css" media="screen" />				
		<script src="{$baseURL}/js/jqValidation/jquery.validationEngine-en.js" type="text/javascript"></script>
		<script src="{$baseURL}/js/jqValidation/jquery.validationEngine.js" type="text/javascript"></script>
		
		{literal}
			<script>	
				$(document).ready(function() {
					// SUCCESS AJAX CALL, replace "success: false," by:     success : function() { callSuccessFunction() }, 
					
					$("#form1").validationEngine()
					
					
					//$.validationEngine.loadValidation("#date")
					//alert($("#formID").validationEngine({returnIsValid:true}))
					//$.validationEngine.buildPrompt("#date","This is an example","error")	 		 // Exterior prompt build example								 // input prompt close example
					//$.validationEngine.closePrompt(".formError",true) 							// CLOSE ALL OPEN PROMPTS
				});
			</script>	
		{/literal}	
		<!-- ketchup - Nice Javascript Form errors reporting --> 
		<!--
		<link rel="stylesheet" type="text/css" href="{$baseURL}/js/ketchupValidation/jquery.ketchup.css" media="screen" />				
		
		<script src="{$baseURL}/js/ketchupValidation/jquery.ketchup.js" type="text/javascript"></script>
		<script src="{$baseURL}/js/ketchupValidation/jquery.ketchup.messages.js" type="text/javascript"></script>
		<script src="{$baseURL}/js/ketchupValidation/jquery.ketchup.validations.basic.js" type="text/javascript"></script>
		
			
		{literal}
		
			<script type="text/javascript" language="javascript">
				$(document).ready(function() {
					$('#form1').ketchup();
				});
				
			</script>
		{/literal}	
		-->
		
	</head>
	<body>		
		<div id="container">
			
			<!-- Header -->						
				{include file='./includes/headerPrivada.tpl'}		
		
			<!-- End Header -->
			
			<div class="orangeSlash"></div>
			
			<!-- Content -->
	
				<div id="content">
					
					<!-- Modules Col -->					
						{include file="./includes/menuPrivada.tpl"}					
					<!-- End Modules Col -->
					
					<!-- ContentCol -->
					
					<div id="mainContent">
						
						<div id="mainTitle">
							<h4>Nueva encuesta</h4>							
						</div>
						
						<div id="pasos">
								<a href="#" class="completadoLink"><span class="paso completado">1- Datos b&aacute;sicos</span></a>
								<span class="paso activo">2- Tus preguntas</span>
								<span class="paso pendiente">3- Configuraci&oacute;n</span>
						</div>
						
						<div class="clear"></div>
						
						<div id="formContainer">
							<p class="text">lorem ipsum dolor sit amet et laburumque post senectum est.</p>
							
							<div id="formContent">
							
							<form id="form1" action="#" method="POST">
									<div class="rowElem"><label>Input Text:</label><input type="text" id="inputtext" name="inputtext" class="validate[required,length[0,20]]" /></div>
									<div class="rowElem"><label>Input Password:</label><input id="pass" name="pass" type="password" class="validate[required,length[0,20]]"  /></div>
									<div class="rowElem"><label>Checkbox Horizontal: </label>
										<input type="checkbox" name="chbox1" id="chbox1" ><label class="uniLineal">Check1</label>
										<input type="checkbox" name="chbox2" id="chbox2" ><label class="uniLineal">Check2</label>
									</div>
									<div class="rowElem"><label>Checkbox Vertical: </label>
										<input type="checkbox" name="chbox3" id="chbox3"><label>Check1</label>
										<input type="checkbox" name="chbox4" id="chbox4"><label>Check2</label>
									</div>
									<div class="rowElem"><label>Radio Horizontal:</label> 
										<input type="radio" id="radio1" name="question" value="oui" ><label class="uniLineal">oui</label>
										<input type="radio" id="radio2" name="question" value="non" ><label class="uniLineal">non</label>
									</div>
									<div class="rowElem"><label>Radio Vertical:</label> 
										<input class="validate[required] radio" type="radio" id="radio3" name="question2" value="oui" ><label>oui</label>
										<input class="validate[required] radio" type="radio" id="radio4" name="question2" value="non" ><label>non</label>
									</div>
									<div class="rowElem"><label>Textarea :</label> <textarea id="tarea" name="tarea" cols="40" rows="12" class="validate[required]" name="mytext"></textarea></div>

									<div class="rowElem">
										<label>Select :</label>
										<select id="select1" name="select1" class="validate[required]" >
											<option value="opt1">1&nbsp;</option>
											<option value="opt2">2&nbsp;</option>
										</select>
									</div>
									<div class="rowElem">
										<label>Select Redimentionn&eacute; :</label>
										<select id="select2" name="select2" class="validate[required]" >
											<option value="opt1">Big Option test line with more wordssss</option>
											<option value="opt2">Option 2</option>
											<option value="opt3">Option 3</option>
											<option value="opt4">Option 4</option>
											<option value="opt5">Option 5</option>
											<option value="opt6">Option 6</option>
											<option value="opt7">Option 7</option>
											<option value="opt8">Option 8</option>
										</select>
									</div>
									
									<div id="botonera">
										<input type="button" value="Volver al Paso1" />
										<input type="submit" value="Guardar cambios" />
										<input type="button" value="Guardar e ir al Paso 3" />
									</div>
																				
								</form>
							
							</div>
							
							
						</div>

							
				</div>								
			
			</div>								
				
			<!-- End Content -->

			<div class="orangeSlash"></div>
			
		<!-- Footer -->			
			{include file='./includes/footer.tpl'}			
		<!-- End Footer -->
			
		</div>		
		
	</body>
</html>