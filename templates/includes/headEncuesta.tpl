
	<!-- Header and metas -->
		<title>Crea tus Encuestas - Nombre de la encuesta</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		
		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="{$baseURL}/css/encuestas.css" media="screen" />
		
		<!-- JQUERY & SLIDES-->
		<script src="{$baseURL}/js/jquery.min.js" type="text/javascript"></script>
		
		<!-- CUFON -->
		<script src="{$baseURL}/js/cufon/cufon-yui.js" type="text/javascript"></script>		
		<script src="{$baseURL}/js/cufon/DINSchrift_700.font.js" type="text/javascript"></script>
		
		
		{literal}
		
			<script type="text/javascript">
		
				Cufon.replace('h1');
				Cufon.replace('h2');
				Cufon.replace('h3');
				Cufon.replace('h4');
				
				Cufon.replace('#copyright');
				Cufon.replace('.registerLink', {hover: true});
							
			</script>		

		{/literal}
	
	