<div class="orangeSlash"></div>
				<div id="ieFilter">
					<div id="ieFilterInfo">
						<div id="filterImg"><img src="{$baseURL}/images/info.jpg" alt="Informaci&oacute;n"></div>
						<div id="filterText">
							<p>Est&aacute;s utilizando un navegador antiguo, con el que no podr&aacute;s visualizar correctamente todos los elementos de esta web.</p>
							<p>Te recomendamos instalar un navegador m&aacute;s reciente:</p>
							<ul id="filterList">
								<li><a href="http://www.google.com/chrome/" target="_blank"><img src="{$baseURL}/images/chromeLogo.jpg" alt="Google Chrome"></a><a href="http://www.google.com/chrome/" target="_blank">Google Chrome</a></li>
								<li><a href="http://www.mozilla.com/es-ES/" target="_blank"><img src="{$baseURL}/images/firefoxLogo.jpg" alt="Firefox"></a><a href="http://www.mozilla.com/es-ES/" target="_blank">Firefox</a></li>
								<li><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx" target="_blank"><img src="{$baseURL}/images/ieLogo.jpg" alt="Internet Explorer 8"></a><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx" target="_blank">Internet Explorer 8</a></li>
							</ul>
						</div>
					</div>
				</div>