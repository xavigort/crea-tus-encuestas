		<div id="header">
				<div id="logoContainer">
					<div id="logo"><a href="{$baseURL}/"><img alt="Crea tus encuestas" src="{$baseURL}/images/logo.jpg" /></a></div>
				</div>
				
				<div id="login">
					{if isset($usuarioPrivado)}
						<a class="loginLink" href="{$baseURL}/cerrar-sesion/"><span class="loginButton">Cerrar sesi&oacute;n</span></a>
						<a class="loginLink" href="{$baseURL}/listado-encuestas/"><span class="loginButton">Tu area privada</span></a>
						<p class="saludo">Bienvido/a {$usuarioPrivado->getNombre()},</p>
					{else}
						<a class="loginLink" href="{$baseURL}/formulario-acceso/"><span class="loginButton">Accede a tu cuenta</span></a>
					{/if}
				</div>
				
				<div id="menu">					
						<a class="menuLink" href="{$baseURL}/demo/"><span class="menuButton">Demo</span></a>
						<a class="menuLink" href="{$baseURL}/plantillas/"><span class="menuButton">Plantillas</span></a>
						<a class="menuLink" href="{$baseURL}/ultimas-encuestas/"><span class="menuButton">&Uacute;ltimas encuestas</span></a>
						<a class="menuLink" href="{$baseURL}/blog/"><span class="menuButton">Blog</span></a>					
				</div>
			</div>