
	<!-- Header and metas -->
		<title>Crea Tus Encuestas</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		
		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="{$baseURL}/css/creatusencuestas.css" media="screen" />				
		
		<!-- JQUERY & SLIDES-->
		<script src="{$baseURL}/js/jquery.min.js" type="text/javascript"></script>
		
		<!-- CUFON -->
		<script src="{$baseURL}/js/cufon/cufon-yui.js" type="text/javascript"></script>		
		<script src="{$baseURL}/js/cufon/DINSchrift_700.font.js" type="text/javascript"></script>
		
		
		{literal}
		
			<script type="text/javascript">
		
				Cufon.replace('h1');
				Cufon.replace('h2');
				Cufon.replace('h3');
				Cufon.replace('h4');
				
				/** Home **/
				
					/** Normal efect **/				
					Cufon.replace('#titleVentajas');
					Cufon.replace('#ventajasListado li');
					Cufon.replace('#navegacion');
					Cufon.replace('.webMapList li');
					Cufon.replace('#copyright');
					
					/** Hover link efect **/
					Cufon.replace('#caracteristicasListado a', {hover: true});
					Cufon.replace('.registerLinks', {hover: true});
					Cufon.replace('.menuLink', {hover: true});
					Cufon.replace('.loginLink', {hover: true});
					Cufon.replace('.webMapLink', {hover: true});

					
				/** Plantillas **/
				
					/** Normal efect **/		
					Cufon.replace('#modulo1Title');
					Cufon.replace('#modulo1Listado li');
					Cufon.replace('#modulo2Title');
				
					/** Hover link efect **/
					Cufon.replace('.registerLink', {hover: true});
				
				/** FAQs **/
				
					/** Normal efect **/		
					Cufon.replace('#modulo3Title');
					
				

						
			</script>		

		{/literal}
	
	