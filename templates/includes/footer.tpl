		<div id="footer">
				
				<div id="footerContainer">
				
					<p id="navegacion">Navegacion</p>
					
					<ul class="webMapList">
						<li><a href="{$baseURL}/formulario-acceso/" class="webMapLink">Login</a></li>
						<li class="webMapListSpacer">-</li>
						<li><a href="{$baseURL}/registro/" class="webMapLink">Registro</a></li>
					</ul>		
					
					<p class="spacer15">&nbsp;</p>
					
					<ul class="webMapList">
						<li><a href="{$baseURL}/demo/" class="webMapLink">Demo</a></li>
						<li class="webMapListSpacer">-</li>
						<li class="active">Plantillas</li>
						<li class="webMapListSpacer">-</li>
						<li><a href="{$baseURL}/ultimas-encuestas/" class="webMapLink">&Uacute;ltimas encuestas</a></li>
						<li class="webMapListSpacer">-</li>
						<li><a href="{$baseURL}/blog/" class="webMapLink">Blog</a></li>
						<li class="webMapListSpacer">-</li>
						<li><a href="{$baseURL}/acerca-de/" class="webMapLink">Acerca de</a></li>
						<li class="webMapListSpacer">-</li>
						<li><a href="{$baseURL}/faqs/" class="webMapLink">FAQs</a></li>
						<li class="webMapListSpacer">-</li>
						<li><a href="{$baseURL}/aviso-legal/" class="webMapLink">Aviso Legal</a></li>
					</ul>
					
					<p id="copyright">Creatusencuestas.com &copy; {literal}<script>var now = new Date();document.write(now.getFullYear());</script>{/literal}</p>
					
				</div>
				
			</div>
		
			<!--[if IE]>
				{literal}	<script type="text/javascript"> Cufon.now(); </script>{/literal}
			<![endif]-->	