<?php
require_once('./sql/mysql.php');
require_once('./sql/encuesta_mysql.php');

class encuestaMySQLMD{
    
  public function encuestaMySQLMD ()
	{
	}
	
	private function fromMySQL($origen)
	{
		$encuesta = createInstance("encuesta");
		
		$encuesta->setId(html_entity_decode($origen['idEncuesta']));
		$encuesta->setIdUsuario(html_entity_decode($origen['idUsuario']));
		$encuesta->setEstadoEncuesta(html_entity_decode($origen['estadoEncuesta']));
		$encuesta->setNombreEncuesta(html_entity_decode($origen['nombreEncuesta']));
		$encuesta->setDescripcionEncuesta(html_entity_decode($origen['descripcionEncuesta']));
		$encuesta->setMostrarProgreso(html_entity_decode($origen['mostrarProgreso']));
		$encuesta->setNumerarPreguntas(html_entity_decode($origen['numerarPreguntas']));
		$encuesta->setTipoFinalizacion(html_entity_decode($origen['tipoFinalizacion']));
		$encuesta->setMensajeFinalizacion(html_entity_decode($origen['mensajeFinalizacion']));
		$encuesta->setModeloEncuesta(html_entity_decode($origen['modeloEncuesta']));
		$encuesta->setControlPorPassword(html_entity_decode($origen['controlPorPassword']));
		$encuesta->setPasswordDeControl(html_entity_decode($origen['passwordDeControl']));
		$encuesta->setIdentificacionUsuarios(html_entity_decode($origen['identificacionUsuarios']));
		$encuesta->setCampoIdentificacion(html_entity_decode($origen['campoIdentificacion']));
		$encuesta->setUsuarioVenResultados(html_entity_decode($origen['usuarioVenResultados']));
		$encuesta->setNotificarEncuestas(html_entity_decode($origen['notificarEncuestas']));
		$encuesta->setEncuestaDestacada(html_entity_decode($origen['encuestaDestacada']));
		$encuesta->setFechaAlta(html_entity_decode($origen['fechaAlta']));
		
		$listadoPreguntas = $this->getPreguntas($encuesta->getId());
		$listadoRespuestas = $this->getRespuestas($encuesta->getId());
		$encuesta->setPreguntas(html_entity_decode($listadoPreguntas));
		$encuesta->setRespuestas(html_entity_decode($listadoRespuestas));
		
		
		return $encuesta;		
	}
	
	private function getPreguntas($idEncuesta)
	{
		return 1;
	}
	
	private function getRespuestas($idEncuesta)
	{
		return 1;
	}
	
	public function getAllEncuestasUsuario($idUsuario)
	{
		$params = array();
		$params['idUsuario'] = $idUsuario;
		$stm = SELECT_ALL_ENCUESTAS_USUARIO;
		
		$link = connect();
		$stm = preparaSQL($stm,$link,$params);
		
		$datos = execute_query_SQL($stm,$link);
		disconnect($link);

		if (count($datos)>0)
		{
			$i=0;
			foreach($datos as $index=>$current)
			{
				$encuestas[$i] = $this->fromMySQL($current);
				$i++;
			}
				
   		return $encuestas;
		}
		else
			return null;
	}
	
	
}

		
?>