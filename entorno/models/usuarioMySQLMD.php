<?php
require_once('./sql/mysql.php');
require_once('./sql/usuario_mysql.php');

class usuarioMySQLMD{
    
  public function usuarioMySQLMD ()
	{
	}
	
	private function fromMySQL($origen)
	{
		$usuario = createInstance("usuario");
		$usuario->setId(html_entity_decode($origen['idUsuario']));
		$usuario->setNombre(html_entity_decode($origen['nombreUsuario']));
		$usuario->setEmpresa(html_entity_decode($origen['empresaUsuario']));
		$usuario->setEmail(html_entity_decode($origen['emailUsuario']));
		$usuario->setPassword(html_entity_decode($origen['passwordUsuario']));
		$usuario->setFechaAlta(html_entity_decode($origen['fechaAltaUsuario']));
		
		return $usuario;		
	}
	
	public function getUserByEmail($email)
	{
		$params = array();
		$params['email'] = $email;
		$stm = SELECT_USUARIO_BY_EMAIL;
		
		$link = connect();
		$stm = preparaSQL($stm,$link,$params);
		$datos = execute_query_SQL($stm,$link);
		disconnect($link);

		if (count($datos)>0)
		{
			$usuario = $this->fromMySQL($datos[0]);
   		return $usuario;
		}
		else
			return null;
	}

  
}

		
?>