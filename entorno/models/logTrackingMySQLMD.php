<?php
require_once('./sql/mysql.php');
require_once('./sql/logTracking_mysql.php');

class logTrackingMySQLMD{
    
  public function logTrackingMySQLMD ()
	{
	}
	
	private function fromMySQL($origen)
	{
		$logTracking = createInstance("logTracking");
		$logTracking->setId(html_entity_decode($origen['id']));
		$logTracking->setLogCode(html_entity_decode($origen['logCode']));
		$logTracking->setUserId(html_entity_decode($origen['userId']));
		$logTracking->setSurveyId(html_entity_decode($origen['surveyId']));
		$logTracking->setDescription(html_entity_decode($origen['description']));
		$logTracking->setDate(html_entity_decode($origen['date']));
		
		return $logTracking;		
	}
	
	public function insertLog($logCode,$userId,$surveyId,$description)
	{
		$params = array();
		$params['logCode'] = $logCode;
		$params['userId'] = $userId;
		$params['surveyId'] = $surveyId;
		$params['description'] = $description;
		
		$stm = INSERT_LOG_TRACKING;
		
		$link = connect();
		$stm = preparaSQL($stm,$link,$params);
		
		execute_SQL($stm,$link);
		disconnect($link);
	}
  
}

		
?>