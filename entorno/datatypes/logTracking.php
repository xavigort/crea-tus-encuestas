<?php

class logTracking
{
	private $id=0;
	private $logCode = 0;
	private $userId = 0;
	private $surveyId = 0;
	private $description = "";
	private $date = "2000-01-01 00:00:00";

  /*
  * Constructor
  */

  function logTracking () 
	{
  } 
  
 /*Getters, setters */
 
  public function getId()
	{
  	  return $this->id;
  }     
  public function setId($val)
	{
     $this->id = $val;
  }
	
	public function getLogCode()
	{
  	  return $this->logCode;
  }     
  public function setLogCode($val)
	{
     $this->logCode = $val;
  }
	
	public function getUserId()
	{
  	  return $this->userId;
  }     
  public function setUserId($val)
	{
     $this->userId = $val;
  }
	
	public function getSurveyId()
	{
  	  return $this->surveyId;
  }     
  public function setSurveyId($val)
	{
     $this->surveyId = $val;
  }
	
	public function getDescription()
	{
  	  return $this->description;
  }     
  public function setDescription($val)
	{
     $this->description = $val;
  }
	
	public function getDate()
	{
  	  return $this->date;
  }     
  public function setDate($val)
	{
     $this->date = $val;
  }
	
}
?>