<?php

class pregunta 
{
	private $idPregunta = 0;
	private $idEncuesta = 0;
	private $tipoPregunta = 0;
	private $nombrePregunta = "";
	private $obligatoriaPregunta = 0;
	private $tamanoPregunta = 0;
	private $tipoInformacion = 0;
	private $alineacionPregunta = 0;
	
	private $valoresPregunta = array();
	
  /*
  * Constructor
  */

  function pregunta() 
	{
  } 
  
 /*Getters, setters */
 
  public function getIdPregunta()
	{
  	  return $this->idPregunta;
  }     
  public function setIdPregunta($val)
	{
     $this->idPregunta = $val;
  }
	
	public function getIdEncuesta()
	{
  	  return $this->idEncuesta;
  }     
  public function setIdEncuesta($val)
	{
     $this->idEncuesta = $val;
  }
	
	public function getTipoPregunta()
	{
  	  return $this->tipoPregunta;
  }     
  public function setTipoPregunta($val)
	{
     $this->tipoPregunta = $val;
  }
	
	public function getNombrePregunta()
	{
  	  return $this->nombrePregunta;
  }     
  public function setNombrePregunta($val)
	{
     $this->nombrePregunta = $val;
  }
	
	public function getObligatoriaPregunta()
	{
  	  return $this->obligatoriaPregunta;
  }     
  public function setObligatoriaPregunta($val)
	{
     $this->obligatoriaPregunta = $val;
  }
	
	public function getTamanoPregunta()
	{
  	  return $this->tamanoPregunta;
  }     
  public function setTamanoPregunta($val)
	{
     $this->tamanoPregunta = $val;
  }
	
	public function getTipoInformacion()
	{
  	  return $this->tipoInformacion;
  }     
  public function setTipoInformacion($val)
	{
     $this->tipoInformacion = $val;
  }
	
	public function getAlineacionPregunta()
	{
  	  return $this->alineacionPregunta;
  }     
  public function setAlineacionPregunta($val)
	{
     $this->alineacionPregunta = $val;
  }
	
	
	public function getValoresPregunta()
	{
  	  return $this->valoresPregunta;
  }     
  public function setValoresPregunta($val)
	{
     $this->valoresPregunta = $val;
  }
	
	
	
}
?>