<?php

class usuario 
{
	private $id=0;
	private $nombre = "";
	private $empresa = "";
	private $email = "";
	private $password = "";
	private $fechaAlta = "2000-01-01 00:00:00";

  /*
  * Constructor
  */

  function usuario () 
	{
  } 
  
 /*Getters, setters */
 
  public function getId()
	{
  	  return $this->id;
  }     
  public function setId($val)
	{
     $this->id = $val;
  }
	
  public function getNombre()
	{
  	  return $this->nombre;
  }     
  public function setNombre($val)
	{
     $this->nombre = $val;
  }
	
  public function getEmpresa()
	{
  	  return $this->empresa;
  }     
  public function setEmpresa($val)
	{
     $this->empresa = $val;
  }
	
  public function getEmail()
	{
  	  return $this->email;
  }     
  public function setEmail($val)
	{
     $this->email = $val;
  }
	
  public function getPassword()
	{
  	  return $this->password;
  }     
  public function setPassword($val)
	{
     $this->password = $val;
  }
	
  public function getFechaAlta()
	{
  	  return $this->fechaAlta;
  }     
  public function setFechaAlta($val)
	{
     $this->fechaAlta = $val;
  }
  
}
?>