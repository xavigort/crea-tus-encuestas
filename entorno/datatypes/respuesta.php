<?php

class respuesta 
{
	private $idRespuesta = 0;
	private $idEncuesta = 0;
	private $idPregunta = 0;
	private $identificadorEncuestado = "";
	private $valorRespuesta = "";
	private $fechaRespuesta = "2000-01-01 00:00:00";;
	
  /*
  * Constructor
  */

  function respuesta()
	{
  } 
  
 /*Getters, setters */
 
  public function getIdRespuesta()
	{
  	  return $this->idRespuesta;
  }     
  public function setIdRespuesta($val)
	{
     $this->idRespuesta = $val;
  }
	
  public function getIdEncuesta()
	{
  	  return $this->idEncuesta;
  }     
  public function setIdEncuesta($val)
	{
     $this->idEncuesta = $val;
  }
	
  public function getIdPregunta()
	{
  	  return $this->idPregunta;
  }     
  public function setIdPregunta($val)
	{
     $this->idPregunta = $val;
  }
	
  public function getIdentificadorEncuestado()
	{
  	  return $this->identificadorEncuestado;
  }     
  public function setIdentificadorEncuestado($val)
	{
     $this->identificadorEncuestado = $val;
  }
	
  public function getValorRespuesta()
	{
  	  return $this->valorRespuesta;
  }     
  public function setValorRespuesta($val)
	{
     $this->valorRespuesta = $val;
  }
	
  public function getFechaRespuesta()
	{
  	  return $this->fechaRespuesta;
  }     
  public function setFechaRespuesta($val)
	{
     $this->fechaRespuesta = $val;
  }
	
  
  
}
?>