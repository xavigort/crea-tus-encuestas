<?php

class encuesta 
{
	private $id = 0;
	private $idUsuario = 0;
	private $estadoEncuesta = 0;
	private $nombreEncuesta = "";
	private $descripcionEncuesta = "";
	private $mostrarProgreso = 0;
	private $numerarPreguntas = 0;
	private $tipoFinalizacion = 0;
	private $mensajeFinalizacion = "";
	private $modeloEncuesta = 0;
	private $controlPorPassword = 0;
	private $passwordDeControl = "";
	private $identificacionUsuarios = 0;
	private $campoIdentificacion = "";
	private $usuarioVenResultados = 0;
	private $notificarEncuestas = 0;
	private $encuestaDestacada = 1;	
	private $fechaAlta = "2000-01-01 00:00:00";
	
	private $preguntas = array();
	private $respuestas = array();
	
  /*
  * Constructor
  */

  function encuesta() 
	{
  } 
  
 /*Getters, setters */
 
  public function getId()
	{
  	  return $this->id;
  }     
  public function setId($val)
	{
     $this->id = $val;
  }
	
  public function getIdUsuario()
	{
  	  return $this->idUsuario;
  }     
  public function setIdUsuario($val)
	{
     $this->idUsuario = $val;
  }
	
  public function getEstadoEncuesta()
	{
  	  return $this->estadoEncuesta;
  }     
  public function setEstadoEncuesta($val)
	{
     $this->estadoEncuesta = $val;
  }
	
  public function getNombreEncuesta()
	{
  	  return $this->nombreEncuesta;
  }     
  public function setNombreEncuesta($val)
	{
     $this->nombreEncuesta = $val;
  }
	
  public function getDescripcionEncuesta()
	{
  	  return $this->descripcionEncuesta;
  }     
  public function setDescripcionEncuesta($val)
	{
     $this->descripcionEncuesta = $val;
  }
	
  public function getMostrarProgreso()
	{
  	  return $this->mostrarProgreso;
  }     
  public function setMostrarProgreso($val)
	{
     $this->mostrarProgreso = $val;
  }
	
  public function getNumerarPreguntas()
	{
  	  return $this->numerarPreguntas;
  }     
  public function setNumerarPreguntas($val)
	{
     $this->numerarPreguntas = $val;
  }
	
  public function getTipoFinalizacion()
	{
  	  return $this->tipoFinalizacion;
  }     
  public function setTipoFinalizacion($val)
	{
     $this->tipoFinalizacion = $val;
  }
	
  public function getMensajeFinalizacion()
	{
  	  return $this->mensajeFinalizacion;
  }     
  public function setMensajeFinalizacion($val)
	{
     $this->mensajeFinalizacion = $val;
  }
	
  public function getModeloEncuesta()
	{
  	  return $this->modeloEncuesta;
  }     
  public function setModeloEncuesta($val)
	{
     $this->modeloEncuesta = $val;
  }
	
  public function getControlPorPassword()
	{
  	  return $this->controlPorPassword;
  }     
  public function setControlPorPassword($val)
	{
     $this->controlPorPassword = $val;
  }
	
  public function getPasswordDeControl()
	{
  	  return $this->passwordDeControl;
  }     
  public function setPasswordDeControl($val)
	{
     $this->passwordDeControl = $val;
  }
	
  public function getIdentificacionUsuarios()
	{
  	  return $this->identificacionUsuarios;
  }     
  public function setIdentificacionUsuarios($val)
	{
     $this->identificacionUsuarios = $val;
  }
	
  public function getCampoIdentificacion()
	{
  	  return $this->campoIdentificacion;
  }     
  public function setCampoIdentificacion($val)
	{
     $this->campoIdentificacion = $val;
  }
	
  public function getUsuarioVenResultados()
	{
  	  return $this->usuarioVenResultados;
  }     
  public function setUsuarioVenResultados($val)
	{
     $this->usuarioVenResultados = $val;
  }
	
  public function getNotificarEncuestas()
	{
  	  return $this->notificarEncuestas;
  }     
  public function setNotificarEncuestas($val)
	{
     $this->notificarEncuestas = $val;
  }
	
  public function getEncuestaDestacada()
	{
  	  return $this->encuestaDestacada;
  }     
  public function setEncuestaDestacada($val)
	{
     $this->encuestaDestacada = $val;
  }
	
  public function getFechaAlta()
	{
  	  return $this->fechaAlta;
  }     
  public function setFechaAlta($val)
	{
     $this->fechaAlta = $val;
  }
	
	public function getPreguntas()
	{
		return $this->preguntas();
	}
	public function setPreguntas($val)
	{
		$this->preguntas = $val;
	}
	
	public function getRespuestas()
	{
		return $this->respuestas();
	}
	public function setRespuestas($val)
	{
		$this->respuestas = $val;
	}
	
  
}
?>