<?php

	header("Pragma: no-cache"); 
	header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past

	require_once('./dispatcherFront.php');
	require_once('./lib/generalFunctions.php');
			
	if(isset($_REQUEST['op']) && $_REQUEST['op']!='') 
		$op = str_replace("-","",$_REQUEST['op']);
	else 
		$op = "home";
	$op = strtolower($op);
	
	$dispatcher=createInstance("dispatcherFront");
	
	if(method_exists($dispatcher,$op))
		executeOperationFront($dispatcher,$op);
	else 
		executeOperationFront($dispatcher,"notFound");
	
?>
