<?php	
	
	require_once('./lib/includes.php');
	
	//error_reporting(E_ALL);
	class dispatcherFront extends controller
	{
		private $vista = "";
		protected $model = "MySQL";
		
		
/***	Constructor ***/

		function __construct() 
		{
			$this->vistaGeneral=createInstance("dispatcherFrontVW");
			
			session_start();
			
			$this->vistaGeneral=createInstance("dispatcherFrontVW");
			
			if(isset($_REQUEST['op']))
				$op = $_REQUEST['op'];
			else
				$op = 'doMain';

			$this->vistaGeneral->setval("op",$op);
			$this->vistaGeneral->setval("contact",0);
		} 
		
		
/*** Frontend Functions ****/
		
		public function home()
		{	
			$areaPrivada = $this->usuarioValido();
			$this->vistaGeneral->displayMain();
		}
		
		public function notFound()
		{	
			$areaPrivada = $this->usuarioValido();
			$this->vistaGeneral->displayNotFound();
		}
		
		public function plantillas()
		{	
			$areaPrivada = $this->usuarioValido();
			$this->vistaGeneral->displayPlantillas();
		}
			
		public function ultimasencuestas()
		{	
			$areaPrivada = $this->usuarioValido();
			$this->encuesta();
			//$this->vistaGeneral->displayUltimasEncuestas();
		}
		
		public function encuesta()
		{	
			$areaPrivada = $this->usuarioValido();
			$this->vistaGeneral->displayEncuesta();
		}
			
		public function faqs()
		{	
			$areaPrivada = $this->usuarioValido();
			$this->vistaGeneral->displayFaqs();
		}
		
		public function avisolegal()
		{	
			$areaPrivada = $this->usuarioValido();
			$this->vistaGeneral->displayAvisoLegal();
		}
		
		public function acercade()
		{	
			$areaPrivada = $this->usuarioValido();
			$this->vistaGeneral->displayAcercaDe();
		}
		
		/*
			formularioacceso
			Mostramos la p�gina de acceso.
			El par�metro "msgError" se arrastra, y le indica a la vista si debe o no mostrar el mensaje de error.
		*/
		public function formularioacceso($msgError = 0)
		{
			$areaPrivada = $this->usuarioValido();
			$this->vistaGeneral->setval("msgError",$msgError);
			$this->vistaGeneral->displayFormularioAcceso();
		}
		
		/*
			validaacceso
			Recibimos por POST las variables "username" y "password".
			Si estas valores corresponden a un usuario real, redirigimos a la p�gina de listado de encuestas.
			En caso contrario volvemos al formulario, marcando a 1 la variable "msgError".
		*/
		public function validaacceso()
		{
			$username = $this->getVal('username');
			$password = $this->getVal('password');
			
			$passMD5 = md5($password);
			
			//Controlamos que los dos par�metros tengan valor			
			if(isset($username) && isset($password) && $username != '' && $password != '')
			{			
				//Comprobamos si existe algun usuario con el email definido, de ser as� recuperamos
				$userCT = createInstance("usuarioCT",$this->model,$this->vista);
				$usuario = $userCT->validateUser($username, $password);
				if(isset($usuario))
				{	
					//Si no exist�a la sesi�n de uprifront, la creamos
					if(!session_is_registered('uprifront'))
						session_register('uprifront');
					
					//Guardamos en sesi�n los datos del usuario logado, y lo trasladamos a la vista
					$_SESSION['uprifront'] = $usuario;
					
					//Registramos el acceso
					$this->registraLog(1,$usuario->getId());
					
					//Asiganci�n del usuario a la vista
					$this->vistaGeneral->setval("usuarioPrivado", $usuario);
					$this->listadoencuestas();
				}
				else
					$this->formularioacceso(1);				
			}
			else
				$this->formularioacceso(1);			
		}
		
		/*
			cerrarsesion - Destruimos la sesi�n del usuario
		*/
		public function cerrarsesion()
		{
			if(isset($_SESSION['uprifront']) && $_SESSION['uprifront']!='')
			{
				$this->registraLog(2,$_SESSION['uprifront']->getId());
				unset($_SESSION['uprifront']);
				$this->vistaGeneral->setVal("usuarioFront",NULL);			
			}			
			$this->home();
		}		
		
		
/*** Backend Functions ****/		
		
		public function listadoencuestas()
		{	
			if($this->usuarioValido())
			{
				//Obtenemos el usuario actual
				$usuario = $this->usuarioSesion();
				
				//Obtenemos las encuestas del usuario
				$encuestaCT = createInstance("encuestaCT",$this->model,$this->vista);
				$listadoEncuestas = $encuestaCT->getAllEncuestasUsuario($usuario->getId());
				
				//Asignaciones y desplegamos la vista
				$this->vistaGeneral->setVal("listadoEncuestas",$listadoEncuestas);					
				$this->vistaGeneral->displayListadoEncuestas();
			}
			else
				$this->formularioacceso($msgError);
		}
		
		public function formencuestas1()
		{	
			if($this->usuarioValido())
				$this->vistaGeneral->displayFormEncuestas1();
			else
				$this->formularioacceso($msgError);
		}
		
		public function formencuestas2()
		{	
			if($this->usuarioValido())
				$this->vistaGeneral->displayFormEncuestas2();
			else
				$this->formularioacceso($msgError);
		}
		
		public function formencuestas3()
		{	
			if($this->usuarioValido())
				$this->vistaGeneral->displayFormEncuestas3();
			else
				$this->formularioacceso($msgError);
		}
		
		public function formcampos()
		{	
			if($this->usuarioValido())
				$this->vistaGeneral->displayFormCampos();
			else
				$this->formularioacceso($msgError);
		}
		
		
/*** Auxiliar functions ****/
		
		/*
			usuarioValido - Controla si existe un usuario en sesi�n
		*/		
		private function usuarioValido()
		{
			if(isset($_SESSION['uprifront']) && $_SESSION['uprifront']->getId()!='' && $_SESSION['uprifront']->getId()!=0)
			{
				$this->vistaGeneral->setVal("usuarioPrivado",$_SESSION['uprifront']);
				return 1;
			}
			else
				return 0;
		}
		
		/*
			usuarioSesion - Retorna el usuario que tenemos en sesi�n
		*/		
		private function usuarioSesion()
		{
			return $_SESSION['uprifront'];
		}
		
		/*
			registraLog - Registra en la BD el log con la acci�n y datos que recibe
		*/		
		private function registraLog($logCode,$userId=0,$surveyId=0,$description='')
		{
			$logTrackingCT = createInstance("logTrackingCT",$this->model,$this->vista);
			$logTrackingCT->model->insertLog($logCode,$userId,$surveyId,$description);
		}
		
/*** Class Methods ***/
		
		public function doNew(){}
		public function doModify(){}
		public function doDelete(){}
		public function doList(){}
	}
?>