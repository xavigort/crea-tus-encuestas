<?php

	require_once('./lib/config.php');
	require_once('./lib/defines.php');
	
	require_once('./controllers/controller.php');
	
	include("../smarty/Smarty.class.php");	
	require_once('./views/view.php');
	require_once('./views/smartyVW.php');
	require_once('./views/dispatcherFrontVW.php');	
	
	require_once('./lib/uploads.class.php');
	require_once('./lib/imatges.class.php');
	require_once('./lib/opcionesListado.php');

	require_once('./datatypes/usuario.php');
	require_once('./datatypes/encuesta.php');
	require_once('./datatypes/logTracking.php');
	
	require_once('./controllers/encuestaCT.php');
	require_once('./controllers/usuarioCT.php');
	require_once('./controllers/logTrackingCT.php');
					

?>