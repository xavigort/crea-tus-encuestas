<?php

class opcionesListado
{
  private $PrimerRegistre=1; 
  private $MaxRegistres; 
  private $PaginesTotal;
  private $RegistresTotal; 
  private $PaginaActual;
  
  private $campOrdenacio;
  private $sentitOrdenacio;
  
  private $search;
  private $iniDate;
  private $endDate;
  
  private $defaultIniDate;
  private $defaultEndDate;  		
	
	
  private $idCategoria=0;
	
	function __construct($maxReg) 
  {
  	
		require_once('./lib/defines.php');
		$this->MaxRegistres=$maxReg;		 
		$this->defaultIniDate = DEFAULT_INI_DATE;	
		$this->defaultEndDate = DEFAULT_END_DATE;	
  }	 
  
  public function getPrimerRegistre() { return $this->PrimerRegistre; } 
  public function getMaxRegistres() { return $this->MaxRegistres; } 
  public function getPaginesTotal() { return $this->PaginesTotal; } 
  public function getRegistresTotal() { return $this->RegistresTotal; } 
  public function getPaginaActual() { return $this->PaginaActual; } 
  public function setPrimerRegistre($x) { $this->PrimerRegistre = $x; } 
  public function setMaxRegistres($x) { $this->MaxRegistres = $x; } 
  public function setPaginesTotal($x) { $this->PaginesTotal = $x; } 
  public function setRegistresTotal($x) { $this->RegistresTotal = $x; } 
  public function setPaginaActual($x) { $this->PaginaActual = $x; } 
  
  public function getCampOrdenacio() { return $this->campOrdenacio; } 
  public function getSentitOrdenacio() { return $this->sentitOrdenacio; } 
  public function setCampOrdenacio($x) { $this->campOrdenacio = $x;  } 
  public function setSentitOrdenacio($x) { $this->sentitOrdenacio = $x; } 

  public function getSearch() { return $this->search;}  
  public function setSearch($x) { $this->search = $x; }
  
	public function getIniDate() { return $this->iniDate;}  
  public function setIniDate($x) { $this->iniDate = $x; } 
  public function getEndDate() { return $this->endDate;}  
  public function setEndDate($x) { $this->endDate = $x; } 

  public function getDefaultIniDate() { return $this->defaultIniDate;}  
  public function getDefaultEndDate() { return $this->defaultEndDate;}  
  
	public function setIdCategoria($x) { $this->idCategoria = $x; } 
  public function getIdCategoria() { return $this->idCategoria; } 
 
  public function getPagination()
  { 		
		$j=0;		
		//Si conv� carreguem el link a l'anterior
		if($this->PaginaActual>1)
		{
			$paginacio[$j]=array("numpagina" => -1, "startat" =>($this->PaginaActual-2)*($this->MaxRegistres));	
			$j++;
		}
		
		//Controlem el cas de la plana 1 i si cal o no pintar els "..."
		$paginacio[$j]=array("numpagina" => 1, "startat" => 0);	
		$j++;
		
		if($this->PaginaActual>4)
		{	
			$paginacio[$j]=array("numpagina" => 0, "startat" => 0);	
			$j++;
		}	
		
		//Controlem totes les planes intermitges a pintar		
		for ($i=1;$i<=$this->PaginesTotal-2;$i++)
		{
			if($this->PaginaActual-4<$i && $i<$this->PaginaActual+2)
			{
				$paginacio[$j]=array("numpagina" => $i+1, "startat" =>$i*($this->MaxRegistres));	
				$j++;				
			}			
		}	
		
		//Controlem el cas de la darrera plana i si cal o no pintar els "..."
		if($this->PaginaActual<$this->PaginesTotal-3 && $this->PaginesTotal>4)
		{
			$paginacio[$j]=array("numpagina" => 0, "startat" => 0);	
			$j++;
		}
		
		if($j>0 && $this->PaginesTotal>1)
		{
			$paginacio[$j]=array("numpagina" => $this->PaginesTotal, "startat" =>($this->PaginesTotal-1)*($this->MaxRegistres));	
			$j++;
		}
		
		//Si conv� carreguem tamb� el link a seg�ent
		if($this->PaginaActual<$this->PaginesTotal)
		{
			$paginacio[$j]=array("numpagina" => -2, "startat" =>($this->PaginaActual)*($this->MaxRegistres));	
			$j++;
		}
		
		return $paginacio;
  }  
  
  public function calcPages()
  {
		if(fmod($this->RegistresTotal,$this->MaxRegistres)==0)
		{
			$this->PaginesTotal = (int)($this->RegistresTotal/$this->MaxRegistres);
		}
    else
		{
			$this->PaginesTotal = (int)($this->RegistresTotal/$this->MaxRegistres)+1;		
		}
		
		$this->PaginaActual = intDivide($this->PrimerRegistre,$this->MaxRegistres)+1;		
  }
  
  public function actualitza($paginaActual=1,$totalRegistres)
  {
		$this->numPaginaActual=$paginaActual;
		$this->numRegistresTotal=$totalRegistres;
		
		if(fmod($totalRegistres,$this->numMaxRegistres)==0)
		 $this->numPaginesTotal = (int)($numRegistresTotal/$numMaxRegistres);
		else
		 $this->numPaginesTotal = (int)($numRegistresTotal/$numMaxRegistres)+1;
  }
 
}
