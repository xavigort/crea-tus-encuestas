<?php
class Imagenes extends Uploads{
	var $imagen;
	var $ancho;
	var $alto;
	var $tama�o;
	var $calidad;
	
	function Imagenes(){
		parent::Uploads();
		$this->calidad = 85;
	}
	
	
	function setCalidad($cal){
		$this->calidad = $cal;
	}
	
	function getAnchoImg(){
		return $this->ancho;
	}
	
	function getAltoImg(){
		return $this->alto;
	}
	
		  function crea_tb($jpgFile, $width) {
			   // Get new dimensions
			   list($width_orig, $height_orig) = getimagesize($jpgFile);
			   $height = (int) (($width / $width_orig) * $height_orig);
			
			   // Resample
			   $image_p = imagecreatetruecolor($width, $height);
				switch($this->getExtension())
			   {
				   case 'gif' :
					   $image = imagecreatefromgif($jpgFile);
					   break;
				   case 'png' :
					   $image = imagecreatefrompng($jpgFile);
					   break;
				   case 'jpg' :
					   $image = imagecreatefromjpeg($jpgFile);
					   break;
				   case 'jpeg' :
					   $image = imagecreatefromjpeg($jpgFile);
					   break;
				   default : 
					   die ("ERROR; UNSUPPORTED IMAGE TYPE");
					   break;
			   }
			   imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
			
			   // Output
			   imagejpeg($image_p, $jpgFile."_tb.jpg", $this->calidad);
            
	}
}
?>