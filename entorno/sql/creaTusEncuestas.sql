-- phpMyAdmin SQL Dump
-- version 2.10.2
-- http://www.phpmyadmin.net
-- 
-- Servidor: localhost
-- Tiempo de generaci�n: 14-06-2010 a las 21:16:05
-- Versi�n del servidor: 5.0.45
-- Versi�n de PHP: 5.2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Base de datos: `creatusencuestas`
-- 

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `encuestas`
-- 

CREATE TABLE `encuestas` (
  `idEncuesta` int(11) NOT NULL auto_increment,
  `idUsuario` int(11) NOT NULL,
  `estadoEncuesta` int(1) NOT NULL,
  `nombreEncuesta` varchar(250) collate utf8_bin NOT NULL,
  `descripcionEncuesta` text collate utf8_bin NOT NULL,
  `mostrarProgreso` tinyint(1) NOT NULL default '0',
  `numerarPreguntas` tinyint(1) NOT NULL default '0',
  `tipoFinalizacion` tinyint(1) NOT NULL default '0',
  `mensajeFinalizacion` varchar(250) collate utf8_bin default NULL,
  `modeloEncuesta` int(11) NOT NULL,
  `controlPorPassword` tinyint(1) NOT NULL default '0',
  `passwordDeControl` varchar(100) collate utf8_bin default NULL,
  `identificacionUsuarios` int(1) NOT NULL,
  `campoIdentificacion` varchar(100) collate utf8_bin default NULL,
  `usuarioVenResultados` tinyint(1) NOT NULL default '0',
  `notificarEncuestas` tinyint(1) NOT NULL default '0',
  `encuestaDestacada` tinyint(1) NOT NULL default '1',
  `fechaAlta` datetime NOT NULL,
  PRIMARY KEY  (`idEncuesta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `encuestas`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `encuestas_preguntas`
-- 

CREATE TABLE `encuestas_preguntas` (
  `idPregunta` int(11) NOT NULL auto_increment,
  `idEncuesta` int(11) NOT NULL,
  `tipoPregunta` int(1) NOT NULL,
  `nombrePregunta` varchar(250) collate utf8_bin NOT NULL,
  `obligatoriaPregunta` tinyint(1) NOT NULL default '0',
  `tamanoPregunta` int(1) default NULL,
  `tipoInformacion` int(1) default NULL,
  `alineacionPregunta` tinyint(1) default '0',
  PRIMARY KEY  (`idPregunta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `encuestas_preguntas`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `encuestas_respuestas`
-- 

CREATE TABLE `encuestas_respuestas` (
  `idRespuesta` int(11) NOT NULL auto_increment,
  `idEncuesta` int(11) NOT NULL,
  `idPregunta` int(11) NOT NULL,
  `identificadorEncuestado` varchar(250) collate utf8_bin NOT NULL,
  `valorRespuesta` text collate utf8_bin NOT NULL,
  `fechaRespuesta` datetime NOT NULL,
  PRIMARY KEY  (`idRespuesta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `encuestas_respuestas`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `encuestas_valorespreguntas`
-- 

CREATE TABLE `encuestas_valorespreguntas` (
  `idPreguntaValor` int(11) NOT NULL auto_increment,
  `idPregunta` int(11) NOT NULL,
  `valor` varchar(250) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`idPreguntaValor`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `encuestas_valorespreguntas`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `logtracking`
-- 

CREATE TABLE `logtracking` (
  `id` int(20) NOT NULL auto_increment,
  `logCode` int(2) NOT NULL default '0',
  `userId` int(11) default '0',
  `surveyId` int(11) default '0',
  `description` varchar(255) collate utf8_bin default '',
  `date` datetime NOT NULL default '2000-01-01 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

-- 
-- Volcar la base de datos para la tabla `logtracking`
-- 

INSERT INTO `logtracking` VALUES (1, 1, 1, 0, '', '2010-06-14 20:46:22');
INSERT INTO `logtracking` VALUES (2, 1, 1, 0, '', '2010-06-14 21:01:43');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `logtype`
-- 

CREATE TABLE `logtype` (
  `id` int(11) NOT NULL,
  `name` varchar(25) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 
-- Volcar la base de datos para la tabla `logtype`
-- 

INSERT INTO `logtype` VALUES (0, 0x556e646566696e6564);
INSERT INTO `logtype` VALUES (1, 0x4c6f67696e);
INSERT INTO `logtype` VALUES (2, 0x4c6f676f7574);
INSERT INTO `logtype` VALUES (3, 0x4e657720737572766579);
INSERT INTO `logtype` VALUES (4, 0x4564697420537572766579);
INSERT INTO `logtype` VALUES (5, 0x44656c65746520737572766579);
INSERT INTO `logtype` VALUES (6, 0x5075626c69736820737572766579);
INSERT INTO `logtype` VALUES (7, 0x556e207075626c69736820737572766579);
INSERT INTO `logtype` VALUES (8, 0x53707265616420737572766579);
INSERT INTO `logtype` VALUES (9, 0x4e65772075736572);
INSERT INTO `logtype` VALUES (10, 0x44656c657465642075736572);
INSERT INTO `logtype` VALUES (11, 0x4564697465642075736572);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `modelos_encuestas`
-- 

CREATE TABLE `modelos_encuestas` (
  `idModelo` int(11) NOT NULL auto_increment,
  `nombreModelo` varchar(250) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`idModelo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `modelos_encuestas`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `plantillas_encuestas`
-- 

CREATE TABLE `plantillas_encuestas` (
  `idPlantilla` int(11) NOT NULL auto_increment,
  `nombrePlantilla` varchar(250) collate utf8_bin NOT NULL,
  `descripcionPlantilla` text collate utf8_bin NOT NULL,
  PRIMARY KEY  (`idPlantilla`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `plantillas_encuestas`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `usuarios`
-- 

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL auto_increment,
  `nombreUsuario` varchar(250) collate utf8_bin NOT NULL,
  `empresaUsuario` varchar(250) collate utf8_bin NOT NULL,
  `emailUsuario` varchar(250) collate utf8_bin NOT NULL,
  `passwordUsuario` varchar(250) collate utf8_bin NOT NULL,
  `fechaAltaUsuario` datetime NOT NULL,
  PRIMARY KEY  (`idUsuario`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `usuarios`
-- 

INSERT INTO `usuarios` VALUES (1, 0x78617669657220676f7274206d656c63686f72, 0x637265612074757320656e63756573746173, 0x78617669676f727440676d61696c2e636f6d, 0x663061633238333561393733396166653131663266343165653735383234393420, '2010-05-24 19:40:40');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `usuariospremium`
-- 

CREATE TABLE `usuariospremium` (
  `idUsuario` int(11) NOT NULL,
  `nCuentaPremium` varchar(100) collate utf8_bin NOT NULL,
  `tipoUsuario` int(1) NOT NULL,
  `fechaAltaPremium` datetime NOT NULL,
  `fechaCaducidadPremium` datetime NOT NULL,
  PRIMARY KEY  (`idUsuario`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 
-- Volcar la base de datos para la tabla `usuariospremium`
-- 

