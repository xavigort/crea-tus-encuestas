<?php

abstract class smartyVW extends view
{
  protected $smarty;
	
	function __construct ()
	{
	  parent::__construct();
	  
	  $this->smarty = new Smarty();
	  $this->smarty->setBaseDir(TEMPLATES_PATH);
	  $this->smarty->compile_check = true;
	  $this->smarty->debugging = false;
	}  

	
	protected function display($plantilla)
	{	
		$this->setVal('errorsList',$this->errors);
		$this->setVal('warningsList',$this->warnings);
		$this->setVal('messagesList',$this->messages);
		$this->setVal('infosList',$this->info);
		
		$this->smarty->display($plantilla.'.tpl');
	}
	
 	public function setVal($name, $value)
	{
	  $this->smarty->assign($name,$value);	
	}
	
	public function getVal($name)
	{
		return $this->smarty->get_template_vars($name);
	}
	public function sendMail($to, $subject, $body, $params)
	{ 
		require_once('./lib/class.phpmailer.php');
		require_once('./lib/class.smtp.php');
		
		$result = false;
		$mail = new phpmailer();
		$mail->PluginDir = SMTP_PLUGINDIR;
		$mail->Host = SMTP_HOST;
		$mail->Mailer = SMTP_MAILER;
		$mail->FromName = NAME_FROM;
		$mail->From = MAIL_FROM;
			
		$mail->AddAddress($to);
		$mail->Subject = $subject;    	    	
		
		$completeBody = $this->smarty->fetch($body);  
			
		foreach($params as $index => $value)
			$completeBody = str_replace("[[$index]]", $value, $completeBody);		
		
		$mail->Body = $completeBody;
		
		$mail->IsHTML(true);
		$mail->Timeout=20;
		
		$result = $mail->Send();	 	
	}
	
}

?>