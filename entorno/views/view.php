<?php

abstract class view 
{
	protected $errors;
	protected $warnings;
	protected $messages;
	protected $info;
	
	function __construct ()
	{
	   $this->errors= array();
	   $this->warnings= array();
	   $this->messages= array();
	   $this->info= array();
	}
  
  public function addError($error)
	{
	   	$this->errors[]=$error;
	}
	public function addWarning($warning)
	{
	   	$this->warnings[]=$warning;
	}
	public function addMessages($message)
	{
	   	$this->messages[]=$message;
	}
	public function addInfo($info)
	{
	   	$this->info[]=$info;
	}	
	
	public function getErrors()
	{
	    return ($this->errors);
	}
	
	public function getWarnings()
	{
	    return ($this->warnings);
	}	

	public function getMessages()
	{
	    return ($this->messages);
	}

	public function getInfo()
	{
	    return ($this->info);
	}	
	
	//abstract public function getVal($name);
	abstract public function setVal($name, $value);
	
	abstract public function displayNew();
	abstract public function displayModify();
	abstract public function displayDelete();
	abstract public function displayList();
	}

?>