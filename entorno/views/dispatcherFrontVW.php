<?php

class dispatcherFrontVW extends smartyVW
{

	function __construct ()
	{
	  parent::__construct();
		$this->smarty->setBaseDir(FRONT_TEMPLATES_PATH);
	}  
	
	protected function display($plantilla)
	{	
		$this->setVal('errorsList',$this->errors);
		$this->setVal('warningsList',$this->warnings);
		$this->setVal('messagesList',$this->messages);
		$this->setVal('infosList',$this->info);
		
		$this->smarty->assign("baseURL",URL_BASE);
		$this->smarty->assign("url",URL_BASE."/gestion");
		
		$this->smarty->display($plantilla.'.tpl');
	}

	/*** Frontend Functions ****/
	
	public function displayMain(){$this->display('home');}
	public function displayPlantillas(){$this->display('plantillas');}
	public function displayUltimasEncuestas(){$this->display('ultimasEncuestas');}
	public function displayEncuesta(){$this->display('encuesta');}
	
	public function displayFaqs(){$this->display('faqs');}
	public function displayAvisoLegal(){$this->display('avisoLegal');}
	public function displayAcercaDe(){$this->display('acercaDe');}
	public function displayFormularioAcceso(){$this->display('formularioAcceso');}
	
	public function displayNotFound(){$this->display('notFound');}
	
	/*** Backend Functions ****/
	
	public function displayListadoEncuestas(){$this->display('listadoEncuestas');}
	public function displayFormEncuestas1(){$this->display('formEncuestas1');}
	public function displayFormEncuestas2(){$this->display('formEncuestas2');}
	public function displayFormEncuestas3(){$this->display('formEncuestas3');}
	public function displayFormCampos(){$this->display('formCampos');}
	
	
	/*** Predefined Functions ****/
		
	public function displayNew() {  return;}
	public function displayModify()  {  return;}
	public function displayDelete() {  return;}
	public function displayList()  {  return;}
	
}

?>