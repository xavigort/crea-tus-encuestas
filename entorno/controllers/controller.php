<?php

abstract class controller 
{ 
  protected $model;
  protected $view;
  
  function __construct($ModelClassName,$ViewClassName) 
  {
     $this->model=createInstance($ModelClassName);
     $this->view=createInstance($ViewClassName);
  }

	public function getVal($name)
    {
		if(isset($_REQUEST[$name]))
		{
			$val = htmlspecialchars(trim($_REQUEST[$name]));
			return $val;
			
		}
		else return null;
		
    }
	
	public function getValSelectMultiple($name)
    {
		if(isset($_REQUEST[$name]))
		{
			$i=0;	
			$value=$_REQUEST[$name];
			foreach ($value as $v){
				$values[$i]=$v; 
				$i++;
			}
			return $values;
			
		}
		else return null;
		
    }
	
	public function getValFile($name)
    {
		if(isset($_FILES[$name]))
		{ 
			$val1 = htmlspecialchars(trim($_FILES[$name]['name']));
			$val2 = htmlspecialchars(trim($_FILES[$name]['tmp_name']));
			$val3 = htmlspecialchars(trim($_FILES[$name]['type']));
			
			$val4=$val5='';
			if($val3=='image/jpeg' || $val3=='image/gif' || $val3=='image/pjpeg'){ 
				 list($val4, $val5, $type, $atr) = getimagesize(trim($_FILES[$name]['tmp_name']));
			}

			return array($val1,$val2,$val3,$val4,$val5);
		}
		else return array('','','','','');
		
    }

  abstract public function doList();
  abstract public function doNew();
  abstract public function doModify();
  abstract public function doDelete();
 
}

?>