<?php

class usuarioCT extends controller
{ 

  function __construct($model,$view)
  { 
    require_once('./models/usuario'.$model.'MD.php');
  	require_once('./views/'.$view.'usuarioVW.php');
		parent::__construct('usuario'.$model.'MD',$view.'usuarioVW');
	}

	public function validateUser($email, $password)
	{
		$usuario = $this->model->getUserByEmail($email);
				
		if(isset($usuario))
		{
			$passMD5 = md5($password);
			$usuarioPass = trim($usuario->getPassword());
			
			if($passMD5 == $usuarioPass)
				return $usuario;
		}
		
		return $null;
	}
	
	/** Class Methods **/
		
		public function doNew(){}
		public function doModify(){}
		public function doDelete(){}
		public function doList(){}
	
}

?>