<?php

class encuestaCT extends controller
{ 

  function __construct($model,$view)
  { 
    require_once('./models/encuesta'.$model.'MD.php');
  	require_once('./views/'.$view.'encuestaVW.php');
		parent::__construct('encuesta'.$model.'MD',$view.'encuestaVW');
	}
	
	//INSERT PREGUNTA
	//EDIT PREGUNTA
	//DELETE PREGUNTA
	//GET PREGUNTAS
	
	//INSERT RESPUESTA
	//EDIT RESPUESTA
	//DELETE RESPUESTA
	//GET RESPUESTAS
	
	//INSERT ENCUESTA
	//EDIT ENCUESTA
	//DELETE ENCUESTA
	//GET ENCUESTAS
	
	public function getAllEncuestasUsuario($idUsuario)
	{
		$listadoEncuestas = $this->model->getAllEncuestasUsuario($idUsuario);
		return $listadoEncuestas;
	}
	
	/** Class Methods **/
		
		public function doNew(){}
		public function doModify(){}
		public function doDelete(){}
		public function doList(){}
	
}

?>