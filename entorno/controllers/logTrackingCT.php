<?php

class logTrackingCT extends controller
{ 

  function __construct($model,$view)
  { 
    require_once('./models/logTracking'.$model.'MD.php');
  	require_once('./views/'.$view.'logTrackingVW.php');
		parent::__construct('logTracking'.$model.'MD',$view.'logTrackingVW');
	}
	
	/** Class Methods **/
		
		public function doNew(){}
		public function doModify(){}
		public function doDelete(){}
		public function doList(){}
	
}

?>