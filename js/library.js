<!--

// variables de control
var ren = /./;
var rem = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
var ret = /^[0-9 " "]{8,14}$/;
var num = /^[0-9 " "]{1,20}$/;
var re = /^(http|https)(s)?:\/\/\w+(\.\w+)*(-\w+)?\.([a-z]{2,3}|info|mobi|aero|asia|name)(:\d{2,5})?(\/)?((\/).+)?$/;

/* Funci�n que controla el tama�o m�ximo de car�cteres en un text area*/

function ismaxlength(obj){
var mlength=obj.getAttribute? parseInt(obj.getAttribute("maxlength")) : ""
if (obj.getAttribute && obj.value.length>mlength)
obj.value=obj.value.substring(0,mlength)
}
			
/* Funci�n para definir los errores */
function setError(msg, txt){
	setInnerHtml(msg, txt);
	errors = true;
}

/* Funci�n para obtener un objeto */
function getObject(obj){
	return document.getElementById(obj);
}

/* Funci�n para obtener varios objetos por su tag name*/
function getObjects(obj, tag){
	return obj.getElementsByTagName(tag);
}

/* Funci�n para obtener el nodevalue */
function getNodeValue(obj, tag){
	if(getObjects(obj, tag)[0].hasChildNodes())		return getObjects(obj, tag)[0].firstChild.nodeValue;
}

/* Funci�n para assignar la clase a un objeto */
function setClassName(obj, classN){
	obj.className = classN;
}

/* Funci�n para asignar un texto como contenido de un objeto */
function setInnerHtml(obj, txt){
	obj.innerHTML = txt;
}

/* Funci�n que controla el formato de fechas dd/mm/aaaa y retorna 1 o 0 en funci�n de si es o no correcto*/
function isCorrectDate(dateVal)
{
	var mask =/^((([0][1-9]|[12][\d])|[3][01])[-\/]([0][13578]|[1][02])[-\/][1-9]\d\d\d)|((([0][1-9]|[12][\d])|[3][0])[-\/]([0][13456789]|[1][012])[-\/][1-9]\d\d\d)|(([0][1-9]|[12][\d])[-\/][0][2][-\/][1-9]\d([02468][048]|[13579][26]))|(([0][1-9]|[12][0-8])[-\/][0][2][-\/][1-9]\d\d\d)$/;
	if(dateVal=='' || mask.test(dateVal))
		return true;
	else
		return false;
}

function isCorrectEmail(emailVal)
{
	var mask = /^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/; 
  if(emailVal=='' || mask.test(emailVal))
		return true;
	else
		return false;
}
//-->