//Globals
var oldFieldID = 0;
var oldThumbnailId = 1;
var numSelect = 2;
var numRadio = 2;
var numCheck = 2;
var maxFreeOptions = 5;


/***************************************/
/****** SHOW HIDE ELEM. FUNCTIONS ******/
/***************************************/

function showHiddenField(idField)
{
	var field = getObject(idField);
	field.style.display = "block";
	field.style.visibility = "visible";
}

function hideVisibleField(idField)
{
	var field = getObject(idField);
	field.style.display = "none";
	field.style.visibility = "hidden";	
}

function showFormFieldInline(id)
{
	var field = getObject(id);
	field.style.display = "inline";
	field.style.visibility = "visible";	
}

/*****************************************************************
	function seleccionaEncuesta(num) checks/unchecks 
	encuesta number 'num'	
*****************************************************************/
function showLegend(idTitle,option)
{
	var legend = getObject('titleAction_'+idTitle);
	var leyenda = leyenda;
	switch(option)
	{
		case 1:
			leyenda = "<strong>Visualiza -</strong> &iquest;C&oacute;mo ver&aacute;n los dem&aacute;s tu encuesta?";
			break;
		case 2:
			leyenda = "<strong>Edita -</strong> &iquest;Te falta algo? &iexcl;a&uacute;n est&aacute;s a tiempo!";
			break;
		case 3:
			leyenda = "<strong>Publica -</strong> &iexcl;Lleg&oacute; el momento de publicar!";
			break;
		case 4:
			leyenda = "<strong>Despublica -</strong> &iquest;Deseas cerrar ya la participaci&oacute;n?";
			break;
		case 5:
			leyenda = "<strong>Difunde -</strong> &iexcl;Que todos conozcan tu encuesta!";
			break;
		case 500:
			leyenda = "<strong>Difunde -</strong> (&Uacute;nicamente encuestas publicadas)";
			break;
		case 6:
			leyenda = "<strong>Estad&iacute;sticas -</strong> &iexcl;Hora de ver los resultados!";
			break;
		case 600:
			leyenda = "<strong>Estad&iacute;sticas -</strong> (Deber&aacute;s publicarla primero)";
			break;
		case 7:
			leyenda = "<strong>Borra -</strong> &iquest;Deseas eliminar tu encuesta?";
			break;
		case 700:
			leyenda = "<strong>Borra -</strong> (Deber&aacute;s despublicarla primero)";
			break;
		default:
			leyenda = "";
			break;
	}
	
	legend.innerHTML = leyenda;
		
	return false;
}

function clearLegend(idTitle)
{
	var legend = getObject('titleAction_'+idTitle);
	legend.innerHTML = "";
		
	return false;
}

function showLinkLegend(idTitle)
{
	var legend = getObject(idTitle);
	legend.style.visibility = "visible";
		
	return false;
}

function clearLinkLegend(idTitle)
{
	var legend = getObject(idTitle);
	legend.style.visibility = "hidden";
		
	return false;
}

function showField(fieldID)
{
	var step2 = getObject("step2");
	var formFieldsContainer = getObject("formFieldsContainer");
	var field = getObject("field"+fieldID);
	var oldField = getObject("field"+oldFieldID);
	
	field.className = "campo campoActual"+fieldID;
	if(oldFieldID!=0)
	{
		oldField.className = "campo campoTipo"+oldFieldID;
	}
	
	setTimeout("hideVisibleField('step2')",500);
	setTimeout("hideVisibleField('fieldContainer"+oldFieldID+"')",500);
	
	$(formFieldsContainer).fadeOut("slow");		
	$(step2).fadeOut("slow");
	
	setTimeout("showHiddenField('step2')",500);
	setTimeout("showHiddenField('fieldContainer"+fieldID+"')",500);
	
	$(formFieldsContainer).fadeIn("slow");	
	$(step2).fadeIn("slow");

	oldFieldID = fieldID;
}


function addOption(type)
{
	if(type==3)
	{
		if(numSelect >= maxFreeOptions)
		{
			hideVisibleField('selectListContainer');
			showHiddenField('selectListPremium');
		}
		else
		{
			var newId = document.getElementById("idSelect").value;
			newId ++;
			newId --;
			$("#selectList").append("<p id='containerDesplegable3_"+newId+"'><input id='desplegable3_"+newId+"' type='text' name='desplegable3[]' value='opcion"+(newId + 1)+"' /><span class='fieldExplain' onmouseover=\"showLinkLegend('deleteOpcionContainer3_"+newId+"')\" onmouseout=\"clearLinkLegend('deleteOpcionContainer3_"+newId+"')\" onClick=\"deleteOption(3,'containerDesplegable3_"+newId+"')\">-</span><span id='deleteOpcionContainer3_"+newId+"' class='addContainer'>Eliminar opci&oacute;n</span><span class='vspacer5'></span></p>");
			newId ++;
			numSelect++;	
			document.getElementById("idSelect").value = newId;
		}
	}
	else if(type==4)
	{
		if(numCheck >= maxFreeOptions)
		{
			hideVisibleField('checkListContainer');
			showHiddenField('checkListPremium');
		}
		else
		{
			var newId = document.getElementById("idCheck").value;
			newId ++;
			newId --;
			$("#checkList").append("<p id='containerDesplegable4_"+newId+"'><input id='desplegable4_"+newId+"' type='text' name='desplegable4[]' value='opcion"+(newId + 1)+"' /><span class='fieldExplain' onmouseover=\"showLinkLegend('deleteOpcionContainer4_"+newId+"')\" onmouseout=\"clearLinkLegend('deleteOpcionContainer4_"+newId+"')\" onClick=\"deleteOption(4,'containerDesplegable4_"+newId+"')\">-</span><span id='deleteOpcionContainer4_"+newId+"' class='addContainer'>Eliminar opci&oacute;n</span><span class='vspacer5'></span></p>");
			newId ++;
			numCheck++;	
			document.getElementById("idCheck").value = newId;
		}
	}
	else if(type==5)
	{
		if(numRadio >= maxFreeOptions)
		{
			hideVisibleField('radioListContainer');
			showHiddenField('radioListPremium');
		}
		else
		{
			var newId = document.getElementById("idRadio").value;
			newId ++;
			newId --;
			$("#radioList").append("<p id='containerDesplegable5_"+newId+"'><input id='desplegable5_"+newId+"' type='text' name='desplegable5[]' value='opcion"+(newId + 1)+"' /><span class='fieldExplain' onmouseover=\"showLinkLegend('deleteOpcionContainer5_"+newId+"')\" onmouseout=\"clearLinkLegend('deleteOpcionContainer5_"+newId+"')\" onClick=\"deleteOption(5,'containerDesplegable5_"+newId+"')\">-</span><span id='deleteOpcionContainer5_"+newId+"' class='addContainer'>Eliminar opci&oacute;n</span><span class='vspacer5'></span></p>");
			newId ++;
			numRadio++;	
			document.getElementById("idRadio").value = newId;
		}
	}
	else
		return false;
		
	return false;	
}

function deleteOption(type, idField)
{
	$("#"+idField).remove();
	
	if(type==3)
		numSelect--;
	else if(type==4)
		numCheck--;
	else if(type==5)
		numRadio--;	
}


function showThumbnail(newThumbnailId)
{
	var newThumbnail = getObject("styleThumb"+newThumbnailId);
	var oldThumbnail = getObject("styleThumb"+oldThumbnailId);
		
	$(oldThumbnail).fadeOut("slow");
	setTimeout("hideVisibleField('styleThumb"+oldThumbnailId+"')",500);
	
	setTimeout("showHiddenField('styleThumb"+newThumbnailId+"')",500);
		
	oldThumbnailId = newThumbnailId;
}

function showFieldLegend(idField)
{
	var legend = getObject('fieldLegend');
	var leyenda = "";
	switch(idField)
	{
		case 1:
			leyenda = "<strong>Campo de texto -</strong> ipsum dolor sit amet dolor sit amet dolor sit amet ";
			break;
		case 2:
			leyenda = "<strong>P&aacute;rrafo -</strong> lorem ipsum dolor sit dolor sit amet dolor sit  dolor sit amet ";
			break;
		case 3:
			leyenda = "<strong>Desplegable -</strong> lorem ipsum dolor sit amet dolor sit amet ";
			break;
		case 4:
			leyenda = "<strong>Selector m&uacute;ltiple -</strong> lorem ipsum dolor sit amet dolor sit amet ";
			break;
		case 5:
			leyenda = "<strong>Selector &uacute;nico -</strong> lorem ipsum dolor dolor sit lorem  dolor sit  sit amet ";
			break;
		case 6:
			leyenda = "<strong>Selector &uacute;nico -</strong> lorem ipsum dolor dolor sit lorem  sit amet dolor  amet ";
			break;
		default:
			leyenda = "";
			break;
	}
	
	legend.innerHTML = leyenda;
		
	return false;
}

function clearFieldLegend(idField)
{
	var legend = getObject('fieldLegend');
	legend.innerHTML = "";
		
	return false;
}

function showNewField()
{	
	$("#newField").slideToggle("normal");	
	return false;
}