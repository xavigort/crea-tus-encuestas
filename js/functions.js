
//Globals
var oldNum = 1;
var actualNum = 1;

/*****************************************************************
	function showCaracteristica(num) fades out the old 
	"caracterstica", and fades in the one with number 'num'
*****************************************************************/
function showCaracteristica(num)
{
	actualNum = num;	
	
	//Var definition
	
	var container = getObject('caracteristicasContainer');
	var actualItem = getObject('item'+num);
	var oldItem = getObject('item'+oldNum);

	//Set link colors
	
	actualItem.style.color = "#4b5b69";
	oldItem.style.color = "#041825";
	
	//Fade out/in content
	
	$(container).fadeOut(500);		
	
	setTimeout("hide("+oldNum+")",500);
	setTimeout("show("+num+")",500);
	
	$(container).fadeIn(500);	
	oldNum = num;
	
	return false;
}

/*****************************************************************
	function hide(num), hides the "caracteristica" number 'num'
*****************************************************************/
function hide(num)
{
	var caracteristica = getObject('caracteristica'+num);
	caracteristica.style.visibility='hidden';	
	caracteristica.style.display='none';
}

/*****************************************************************
	function show(num), shows the "caracteristica" number 'num'
*****************************************************************/
function show(num)
{
	var caracteristica = getObject('caracteristica'+num);
	caracteristica.style.visibility='visible';
	caracteristica.style.display='block';
}