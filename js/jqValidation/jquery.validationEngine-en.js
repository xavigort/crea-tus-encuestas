

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{    			// Add your regex rules here, you can take telephone as an example
						"regex":"none",
						"alertText":"Este es un campo obligatorio.",
						"alertTextCheckboxMultiple":"Debes seleccionar alguna opci&oacute;n.",
						"alertTextCheckboxe":"Debes confirmar este campo."},
					"length":{
						"regex":"none",
						"alertText":"Entre ",
						"alertText2":" y ",
						"alertText3": " car&aacute;cteres permitidos"},
					"maxCheckbox":{
						"regex":"none",
						"alertText":"No puedes seleccionar tantas opciones."},	
					"minCheckbox":{
						"regex":"none",
						"alertText":"Por favor selecciona ",
						"alertText2":" opciones."},	
					"confirm":{
						"regex":"none",
						"alertText":"Tu campo no coincide"},		
					"telephone":{
						"regex":"/^[0-9\-\(\)\ ]+$/",
						"alertText":"Debes definir un tel&eacute;fono v&aacute;lido."},	
					"email":{
						"regex":"/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/",
						"alertText":"Debes definir un e-mail v&aacute;lido."},	
					"date":{
                         "regex":"/^[0-9]{1,2}\-\[0-9]{1,2}\-\[0-9]{4}$/",
                         "alertText":"El formato de la fehca debe ser DD-MM-AAAA."},
					"onlyNumber":{
						"regex":"/^[0-9\ ]+$/",
						"alertText":"Debe ser un valor num&eacute;rico."},	
					"noSpecialCaracters":{
						"regex":"/^[0-9a-zA-Z]+$/",
						"alertText":"No se permiten car&aacute;cteres especiales."},	
					"ajaxUser":{
						"file":"validateUser.php",
						"extraData":"name=eric",
						"alertTextOk":"* This user is available",	
						"alertTextLoad":"* Loading, please wait",
						"alertText":"* This user is already taken"},	
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":"* This name is already taken",
						"alertTextOk":"* This name is available",	
						"alertTextLoad":"* Loading, please wait"},		
					"onlyLetter":{
						"regex":"/^[a-zA-Z\ \']+$/",
						"alertText":"Solamente puedes definir letras."}
					}	
		}
	}
})(jQuery);

$(document).ready(function() {	
	$.validationEngineLanguage.newLang()
});