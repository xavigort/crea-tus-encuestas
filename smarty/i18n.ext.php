<?php
require("./Smarty.class.php");
 /**
 * Project: Smarty i18n Plugin
 * File: i18n.ext.php
 *
 * $Id: i18n.ext.php,v 1.1 2008/11/24 08:10:23 pesteban Exp $
 */

class i18n extends Smarty {

	var $i18n_dir = 'i18n';
	
	var $i18n = false;
	
function i18n_load ($langFile, $langArray = 'i18n')
{
	if (is_array($this->i18n)) {
		return true;	
	}	

	if ($langFile == '') {
		$this->trigger_error('i18n: $langFile may not be empty');
		return false;	
	}
	
	$i18nFile = sprintf('%s/%s', $this->i18n_dir, $langFile);
	
	// check if file exists
	if (!file_exists($i18nFile)) {
		$this->trigger_error('i18n: language file not found');
		return false;	
	}

	// "load" file
	require_once($i18nFile);

	if (!is_array($$langArray)) {
		$this->trigger_error('i18n: language array does not exist');
		return false;		
	}
	
	$this->i18n = $$langArray;
}

function i18n_unload ()
{
	settype($this->i18n, 'bool');
	$this->i18n = false;
	$this->clear_compiled_tpl();
}
	
}

?>
