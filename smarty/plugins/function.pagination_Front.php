<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty {mailto} function plugin
 *
 * Type:     function<br>
 * Name:     mailto<br>
 * Date:     May 21, 2002
 * Purpose:  automate mailto address link creation, and optionally
 *           encode them.<br>
 * Input:<br>
 *         - address = e-mail address
 *         - text = (optional) text to display, default is address
 *         - encode = (optional) can be one of:
 *                * none : no encoding (default)
 *                * javascript : encode with javascript
 *                * hex : encode with hexidecimal (no javascript)
 *         - cc = (optional) address(es) to carbon copy
 *         - bcc = (optional) address(es) to blind carbon copy
 *         - subject = (optional) e-mail subject
 *         - newsgroups = (optional) newsgroup(s) to post to
 *         - followupto = (optional) address(es) to follow up to
 *         - extra = (optional) extra tags for the href link
 *
 * Examples:
 * <pre>
 * {mailto address="me@domain.com"}
 * {mailto address="me@domain.com" encode="javascript"}
 * {mailto address="me@domain.com" encode="hex"}
 * {mailto address="me@domain.com" subject="Hello to you!"}
 * {mailto address="me@domain.com" cc="you@domain.com,they@domain.com"}
 * {mailto address="me@domain.com" extra='class="mailto"'}
 * </pre>
 * @link http://smarty.php.net/manual/en/language.function.mailto.php {mailto}
 *          (Smarty online manual)
 * @version  1.2
 * @author   Monte Ohrt <monte@ispi.net>
 * @author   credits to Jason Sweat (added cc, bcc and subject functionality)
 * @param    array
 * @param    Smarty
 * @return   string
 */
function smarty_function_pagination_Front($params, &$smarty)
{
	if ((empty($params['paginacio'])) || (empty($params['prefix'])))  {
        return;
    } 
	$categoria = $params['categoria'];
	$paginacio = $params['paginacio'];
	$prefix = $params['prefix'];
	$hack = $params['hack'];
	$modelPaginacio = "";
	$modelPaginacio = $params['modelPaginacio'];
	$strPaginacio ="";
	
	if (count($paginacio) > 1)
	{
		$strPaginacio.="<ul>";

		foreach($paginacio as $index=>$value)
		{
			if($value['numpagina'] == 0)
				$strPaginacio.="<li>...</li><li class='separador'>-</li>";							
			else if($value['numpagina'] == -1)
				$strPaginacio.="<li><a href=\"".$prefix.$categoria."/".$value['startat']."/\">Anterior</a></li><li class='separador'>-</li>";
			else if($value['numpagina'] == -2)
				$strPaginacio.="<li><a href=\"".$prefix.$categoria."/".$value['startat']."/\">Siguiente</a></li>";
			else
			{
				if ($hack->getPaginaActual() == $value['numpagina'])
				{
					$strPaginacio.="<li>".$value['numpagina']."</li>";						
					if($hack->getPaginaActual() != $hack->getPaginesTotal())
						$strPaginacio.="<li class='separador'>-</li>";
				}
				else
					$strPaginacio.="<li><a href=\"".$prefix.$categoria."/".$value['startat']."/\">".$value['numpagina']."</a></li><li class='separador'>-</li>";				
			}
		}
		$strPaginacio.="</ul>";
	}		
	return $strPaginacio;
}



?>
