<?php

/*
* Smarty plugin
* -------------------------------------------------------------
* File:     compiler.i18n.php
* Type:     compiler
* Name:     i18n-stat
* Purpose:  internationalization
* SVN:      $Id: compiler.i18n.php,v 1.1 2008/11/24 08:11:17 pesteban Exp $
* -------------------------------------------------------------
*/

function smarty_compiler_i18n($tag_arg, &$smarty)
{
	$txt = preg_replace("/\r\n|\n|\r/", " ", trim($matches['1'][$i]));
	$i18n = &$GLOBALS['smarty']->i18n;
	
	if (empty($tag_arg)) {
		$smarty->trigger_error('i18n: missing message');
		return false;	
	}
	
	if (!is_array($i18n)) {
		$smarty->trigger_error('i18n: language array empty');
		return false;	
	}


    if (!isset($i18n[$txt]) || $i18n[$txt] == '') {
        return "\necho stripslashes('".addslashes($txt)."');";
    } else {
        return "\necho stripslashes('".addslashes($i18n[$txt])."');";
    }
}
?>