<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty {mailto} function plugin
 *
 * Type:     function<br>
 * Name:     mailto<br>
 * Date:     May 21, 2002
 * Purpose:  automate mailto address link creation, and optionally
 *           encode them.<br>
 * Input:<br>
 *         - address = e-mail address
 *         - text = (optional) text to display, default is address
 *         - encode = (optional) can be one of:
 *                * none : no encoding (default)
 *                * javascript : encode with javascript
 *                * hex : encode with hexidecimal (no javascript)
 *         - cc = (optional) address(es) to carbon copy
 *         - bcc = (optional) address(es) to blind carbon copy
 *         - subject = (optional) e-mail subject
 *         - newsgroups = (optional) newsgroup(s) to post to
 *         - followupto = (optional) address(es) to follow up to
 *         - extra = (optional) extra tags for the href link
 *
 * Examples:
 * <pre>
 * {mailto address="me@domain.com"}
 * {mailto address="me@domain.com" encode="javascript"}
 * {mailto address="me@domain.com" encode="hex"}
 * {mailto address="me@domain.com" subject="Hello to you!"}
 * {mailto address="me@domain.com" cc="you@domain.com,they@domain.com"}
 * {mailto address="me@domain.com" extra='class="mailto"'}
 * </pre>
 * @link http://smarty.php.net/manual/en/language.function.mailto.php {mailto}
 *          (Smarty online manual)
 * @version  1.2
 * @author   Monte Ohrt <monte@ispi.net>
 * @author   credits to Jason Sweat (added cc, bcc and subject functionality)
 * @param    array
 * @param    Smarty
 * @return   string
 */
function smarty_function_pagination($params, &$smarty)
{
	if ((empty($params['paginacio'])) || (empty($params['prefix'])))  {
        return;
    } 
	$paginacio = $params['paginacio'];
	$prefix = $params['prefix'];
	$search = $params['search'];
	$ordenacio = $params['ordenacio'];
	$campOrdenacio = $params['campOrdenacio'];
	$dateIni = $params['dateIni'];
	$dateEnd = $params['dateEnd'];
	$active = $params['active'];
	$tipologia = $params['tipologia'];
	$idioma = $params['idioma'];
	$at = $params['at'];
	$resultat = $params['resultat'];
	$ids_categories_search = $params['ids_categories_search'];
	
	$hack = $params['hack'];
	$strPaginacio ="";
	
	if (count($paginacio) > 1)
	{
		$strPaginacio.="<ul class=\"pagerList\">";

		foreach($paginacio as $index=>$value)
		{
			if($value['numpagina'] == 0)
				$strPaginacio.="<li><span>...</span></li><li class=\"pagerSeparator\">&nbsp;</li>";							
			else if($value['numpagina'] == -1)
				$strPaginacio.="<li><a href=\"".$prefix.$value['startat']."&amp;campOrdenacio=$campOrdenacio&amp;ordenacio=$ordenacio&amp;search=$search&amp;date1=$dateIni&amp;date2=$dateEnd&amp;active=$active&amp;ids_categories_search=$ids_categories_search&amp;tipologia=$tipologia&amp;idioma=$idioma&amp;at=$at&amp;resultat=$resultat\" class=\"textLink\">Anterior</a></li><li class=\"pagerSeparator\">&nbsp;</li>";
			else if($value['numpagina'] == -2)
				$strPaginacio.="<li><a href=\"".$prefix.$value['startat']."&amp;campOrdenacio=$campOrdenacio&amp;ordenacio=$ordenacio&amp;search=$search&amp;date1=$dateIni&amp;date2=$dateEnd&amp;active=$active&amp;ids_categories_search=$ids_categories_search&amp;tipologia=$tipologia&amp;idioma=$idioma&amp;at=$at&amp;resultat=$resultat\" class=\"textLink\">Seg&uuml;ent</a></li>";
			else
			{
				if ($hack->getPaginaActual() == $value['numpagina']) 
					$strPaginacio.="<li><span>".$value['numpagina']."</span></li>";						
				else
					$strPaginacio.="<li><a href=\"".$prefix.$value['startat']."&amp;campOrdenacio=$campOrdenacio&amp;ordenacio=$ordenacio&amp;search=$search&amp;date1=$dateIni&amp;date2=$dateEnd&amp;active=$active&amp;ids_categories_search=$ids_categories_search&amp;tipologia=$tipologia&amp;idioma=$idioma&amp;at=$at&amp;resultat=$resultat\" class=\"textLink\">".$value['numpagina']."</a></li>";
				if($i<(count($paginacio)-1))
				 $strPaginacio.="<li class=\"pagerSeparator\">&nbsp;</li>";					
			}
		}
		$strPaginacio.="</ul>";
	}		
	return $strPaginacio;
}



?>
